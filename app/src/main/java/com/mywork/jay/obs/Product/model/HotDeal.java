
package com.mywork.jay.obs.Product.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotDeal {


    @SerializedName("HotDeals_id")
    @Expose
    private Integer hotDealsId;
    @SerializedName("HotDeals_name")
    @Expose
    private String hotDealsName;
    @SerializedName("VendorMaster_id")
    @Expose
    private Integer vendorMasterId;
    @SerializedName("HotDeals_description")
    @Expose
    private String hotDealsDescription;
    @SerializedName("HotDeals_image")
    @Expose
    private String hotDealsImage;
    @SerializedName("HotDeals_startTime")
    @Expose
    private String hotDealsStartTime;
    @SerializedName("HotDeals_endTime")
    @Expose
    private String hotDealsEndTime;
    @SerializedName("Deal_price")
    @Expose
    private String dealPrice;
    @SerializedName("Deal_discount")
    @Expose
    private String dealDiscount;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("Products_id")
    @Expose
    private Integer productsId;
    @SerializedName("Products_name")
    @Expose
    private String productsName;
    @SerializedName("brand_name")
    @Expose
    private String brandName;
    @SerializedName("Products_Details")
    @Expose
    private String productsDetails;
    @SerializedName("Products_price")
    @Expose
    private String productsPrice;
    @SerializedName("Products_quanity")
    @Expose
    private String productsQuanity;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("parent_id")
    @Expose
    private Object parentId;
    @SerializedName("image_id")
    @Expose
    private Integer imageId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("is_default")
    @Expose
    private String isDefault;
    @SerializedName("is_wished")
    @Expose
    private Boolean isWished;

    public Integer getHotDealsId() {
        return hotDealsId;
    }

    public void setHotDealsId(Integer hotDealsId) {
        this.hotDealsId = hotDealsId;
    }

    public String getHotDealsName() {
        return hotDealsName;
    }

    public void setHotDealsName(String hotDealsName) {
        this.hotDealsName = hotDealsName;
    }

    public Integer getVendorMasterId() {
        return vendorMasterId;
    }

    public void setVendorMasterId(Integer vendorMasterId) {
        this.vendorMasterId = vendorMasterId;
    }

    public String getHotDealsDescription() {
        return hotDealsDescription;
    }

    public void setHotDealsDescription(String hotDealsDescription) {
        this.hotDealsDescription = hotDealsDescription;
    }

    public String getHotDealsImage() {
        return hotDealsImage;
    }

    public void setHotDealsImage(String hotDealsImage) {
        this.hotDealsImage = hotDealsImage;
    }

    public String getHotDealsStartTime() {
        return hotDealsStartTime;
    }

    public void setHotDealsStartTime(String hotDealsStartTime) {
        this.hotDealsStartTime = hotDealsStartTime;
    }

    public String getHotDealsEndTime() {
        return hotDealsEndTime;
    }

    public void setHotDealsEndTime(String hotDealsEndTime) {
        this.hotDealsEndTime = hotDealsEndTime;
    }

    public String getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(String dealPrice) {
        this.dealPrice = dealPrice;
    }

    public String getDealDiscount() {
        return dealDiscount;
    }

    public void setDealDiscount(String dealDiscount) {
        this.dealDiscount = dealDiscount;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getProductsId() {
        return productsId;
    }

    public void setProductsId(Integer productsId) {
        this.productsId = productsId;
    }

    public String getProductsName() {
        return productsName;
    }

    public void setProductsName(String productsName) {
        this.productsName = productsName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductsDetails() {
        return productsDetails;
    }

    public void setProductsDetails(String productsDetails) {
        this.productsDetails = productsDetails;
    }

    public String getProductsPrice() {
        return productsPrice;
    }

    public void setProductsPrice(String productsPrice) {
        this.productsPrice = productsPrice;
    }

    public String getProductsQuanity() {
        return productsQuanity;
    }

    public void setProductsQuanity(String productsQuanity) {
        this.productsQuanity = productsQuanity;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public Object getParentId() {
        return parentId;
    }

    public void setParentId(Object parentId) {
        this.parentId = parentId;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean getIsWished() {
        return isWished;
    }

    public void setIsWished(Boolean isWished) {
        this.isWished = isWished;
    }
}
