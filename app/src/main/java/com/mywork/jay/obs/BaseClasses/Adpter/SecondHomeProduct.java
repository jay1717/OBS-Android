package com.mywork.jay.obs.BaseClasses.Adpter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mywork.jay.obs.BaseClasses.Model.HomeModel;
import com.mywork.jay.obs.BaseClasses.Model.HomeProduct;
import com.mywork.jay.obs.BaseClasses.util.Log;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealsDetailActivity;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jay on 1/3/18.
 */

    public class SecondHomeProduct extends RecyclerView.Adapter<SecondHomeProduct.SecontHomeProductHolder> {
    private Context context;
    List<HomeProduct> homeProducts;

    public SecondHomeProduct(Context context, List<HomeProduct> homeProducts) {
        this.context = context;
        this.homeProducts = homeProducts;
    }


    @Override
    public SecontHomeProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_second_home_product,parent,false);
        return new SecontHomeProductHolder(view);
    }

    @Override
    public void onBindViewHolder(SecontHomeProductHolder holder, final int position) {
        Picasso.with(context).load(homeProducts.get(position).getCategoryImage()).placeholder(R.drawable.placeholder_1140x350).into(holder._img_product);

        holder.product_txtBrand.setText(homeProducts.get(position).getCategoryName());

//        holder.llvBestShopping.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, HotDealsDetailActivity.class);
//                intent.putExtra("product_name", homeProducts.get(position).getCategoryName());
//                intent.putExtra("product_id", homeProducts.get(position).getCategoryId());
//                context.startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return homeProducts.size();
    }

    public class SecontHomeProductHolder extends RecyclerView.ViewHolder {
        private TextView product_txtBrand;
        private ImageView _img_product;
        private LinearLayout llvBestShopping;
        public SecontHomeProductHolder(View itemView) {
            super(itemView);
            product_txtBrand = (TextView)itemView.findViewById(R.id.strip_secondhome_product_txtBrand);
            _img_product = (ImageView)itemView.findViewById(R.id.strip_secnd_home_product_img_product);
            llvBestShopping = (LinearLayout)itemView.findViewById(R.id.llvBestShopping);
        }
    }
}
