package com.mywork.jay.obs.Product.Navigation.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.DashBoardActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.LoginActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.R;
import com.rm.rmswitch.RMSwitch;

public class Setting extends CoreActivity implements View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    private LinearLayout ll_tc,ll_tnc,ll_cs,ll_fb;
    public DrawerLayout drawer;
    public NavigationView navigationView;
    private ImageView imgEdit,imgMenu;
    private RMSwitch push_notification,email_notification;
    private TextView txtTitle;
    private Pref_Common pref_common;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_setting);
        pref_common = new Pref_Common(Setting.this);
        initBottomTab();
        setBottomTabSelected(4);
        setIds();
        setValue();
        setClickEvents();
        setClickEvent();
        setSliderDashboard(R.menu.navigation_drawer);
    }
    private void setIds(){
        try {
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            txtTitle= (TextView)findViewById(R.id.txtTitle);
            imgEdit = (ImageView)findViewById(R.id.imgEdit);
            imgMenu = (ImageView) findViewById(R.id.imgMenu);
            push_notification = (RMSwitch) findViewById(R.id.rm_switch2);
            email_notification = (RMSwitch) findViewById(R.id.rm_switch3);
            ll_tc = (LinearLayout)findViewById(R.id.ll_tc);
            ll_tnc = (LinearLayout)findViewById(R.id.ll_tnc);
            ll_cs = (LinearLayout)findViewById(R.id.ll_cs);
            ll_fb = (LinearLayout)findViewById(R.id.ll_fb);

            push_notification.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
                @Override
                public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {

                }
            });
            email_notification.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
                @Override
                public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {



                }
            });
            nav_setValue();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setClickEvents() {
        try {
            imgMenu.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setClickEvent(){
        try {
            ll_tc.setOnClickListener(this);
            ll_tnc.setOnClickListener(this);
            ll_cs.setOnClickListener(this);
            ll_fb.setOnClickListener(this);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void nav_setValue(){
        try {
            assert navigationView != null;
            navigationView.setNavigationItemSelectedListener(this);
            setNavigationView(navigationView);
            setDrawerLayout(drawer);
            SetImageMenuShow(imgMenu);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setValue(){
        try {
            txtTitle.setText("Setting");
            txtTitle.setTextColor(getResources().getColor(R.color.colorBlack));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_tc:
                Intent intent = new Intent(Setting.this,AboutUsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.ll_tnc:
                Intent tc = new Intent(Setting.this,TermAndConditionActivity.class);
                startActivity(tc);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.ll_cs:
                Intent cs = new Intent(Setting.this,ContactSupportActivity.class);
                startActivity(cs);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
       case R.id.ll_fb:
//                Intent fb = new Intent(Setting.this,HotDealsDetailActivity.class);
//                startActivity(fb);
//                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.imgMenu:
                MenuClick();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_Home:
                Intent home = new Intent(Setting.this, DashBoardActivity.class);
                startActivity(home);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.hot_deal:
                Intent intent = new Intent(Setting.this, HotDealActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_Product:
                Intent product = new Intent(Setting.this, ProductMainActivity.class);
                startActivity(product);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_message:
                String business_number = pref_common.loadPrefString("business_number");
                String business_name = pref_common.loadPrefString("business_name");
                String business_email = pref_common.loadPrefString("business_email");
                Log.e("business_number",business_number);


                String smsNumber = "91 " +business_number; // E164 format without '+' sign
                Log.e("smsNumber",smsNumber);

                smsNumber = smsNumber.replace("+", "").replace("", "");
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello " + business_name + "!");
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net"); //phone number without "+" prefix
                sendIntent.setPackage("com.whatsapp");
                if (sendIntent.resolveActivity(Setting.this.getPackageManager()) == null) {
                    pref_common.showAlert("WhatsApp not Installed");

                }
                startActivity(sendIntent);
                break;
            case R.id.nav_profile:
                Intent profile = new Intent(Setting.this, ProfileActivity.class);
                startActivity(profile);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_setting:

                break;

            case R.id.nav_logout:
                ALERT_DIALOG_LOGOUT();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
    private void ALERT_DIALOG_LOGOUT() {
        new AlertDialog.Builder(Setting.this)
                .setTitle("Alert!")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Setting.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                        pref_common.savePrefString("islogin", "2");

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)

                .show();
    }
}
