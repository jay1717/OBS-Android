package com.mywork.jay.obs.Product.Navigation.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.DashBoardActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.LoginActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.Product.Adpter.ProductAdpter;
import com.mywork.jay.obs.Product.model.Product;
import com.mywork.jay.obs.Product.model.ProductModel;
import com.mywork.jay.obs.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductMainActivity extends CoreActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, PurchasesUpdatedListener {

    private RecyclerView first_product, second_product;
    private ProductAdpter mAdpter;
    private TextView txtTitle;
    public DrawerLayout drawer;
    public NavigationView navigationView;
    private ImageView imgMenu, imgcart;
    private LinearLayoutManager recyclerViewlayoutManager;
    private ArrayList<ProductModel> productModels;
    List<Product> productList;
    private Pref_Common pref_common;

    BillingClient billingClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_product_main_);
        pref_common = new Pref_Common(ProductMainActivity.this);
        setIds();
        setClickEvents();
        initBottomTab();
        setBottomTabSelected(1);
        setSliderDashboard(R.menu.navigation_drawer);
//        setBaseActivity(DashBoardActivity.this);
        activity = ProductMainActivity.this;
        setSecondAdper();
        setBiling();
    }

    private void setBiling() {
        billingClient = BillingClient.newBuilder(this)
                .enablePendingPurchases()
                .setListener(this).build();


        billingClient.startConnection(new BillingClientStateListener() {


            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    GET_ALL_PRODUCT(billingClient);
                    Purchase.PurchasesResult pr = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
                    List<Purchase> pList = pr.getPurchasesList();
                    for (Purchase iitem : pList) {
                        ConsumeParams consumeParams = ConsumeParams.newBuilder()
                                .setPurchaseToken(iitem.getPurchaseToken())
                                .build();
                        billingClient.consumeAsync(consumeParams, consumeResponseListener);
                    }
                    Toast.makeText(ProductMainActivity.this, "Success to connect", Toast.LENGTH_SHORT).show();

                    // process the purchase
                } else {
                    Toast.makeText(ProductMainActivity.this, "" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onBillingServiceDisconnected() {
                Toast.makeText(ProductMainActivity.this, "Success to disconnect", Toast.LENGTH_SHORT).show();
            }

        });
    }

    ConsumeResponseListener consumeResponseListener = new ConsumeResponseListener() {
        @Override
        public void onConsumeResponse(BillingResult billingResult, String s) {

            AcknowledgePurchaseParams acknowledgePurchaseParams =
                    AcknowledgePurchaseParams.newBuilder()
                            .setPurchaseToken(s)
                            .build();
            billingClient.acknowledgePurchase(acknowledgePurchaseParams, acknowledgePurchaseResponseListener);
        }
    };

    AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener = new AcknowledgePurchaseResponseListener() {
        @Override
        public void onAcknowledgePurchaseResponse(BillingResult billingResult) {

        }
    };


    private void setIds() {
        try {
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            second_product = (RecyclerView) findViewById(R.id.product_activity_second_product);
            txtTitle = (TextView) findViewById(R.id.txtTitle);
            imgMenu = (ImageView) findViewById(R.id.imgMenu);
            imgcart = (ImageView) findViewById(R.id.imgcart);
            txtTitle.setText("Product");
            setValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickEvents() {
        try {
            imgMenu.setOnClickListener(this);
            imgcart.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValue() {
        try {
            imgcart.setVisibility(View.VISIBLE);
            assert navigationView != null;
            navigationView.setNavigationItemSelectedListener(this);
            setNavigationView(navigationView);
            setDrawerLayout(drawer);
            SetImageMenuShow(imgMenu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSecondAdper() {
        try {
            productModels = new ArrayList<>();
            productList = new ArrayList<>();
            recyclerViewlayoutManager = new LinearLayoutManager(ProductMainActivity.this, LinearLayoutManager.VERTICAL, false);
            second_product.setLayoutManager(recyclerViewlayoutManager);
            second_product.setHasFixedSize(true);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GET_ALL_PRODUCT(final BillingClient billingClient) {
        String product_url = BaseApi.postObsAPI(BaseApi.POST_SHOW_CATEGORY);

        Map<String, String> param = new HashMap<>();

        new OBSRequest(ProductMainActivity.this, product_url, OBSRequest.METHOD_GET, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {

                for (int i = 0; i < productModels.size(); i++) {
                    productList = productModels.get(i).getProducts();
                }

                Type type = new TypeToken<ArrayList<ProductModel>>() {
                }.getType();
                productModels = (ArrayList<ProductModel>) new Gson().fromJson(response.toString(), type);


                mAdpter = new ProductAdpter(ProductMainActivity.this, productModels, billingClient);
                second_product.setAdapter(mAdpter);
            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgMenu:
                MenuClick();
                break;
            case R.id.imgcart:
                Intent intent = new Intent(ProductMainActivity.this, CartActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_Home:
                Intent home = new Intent(ProductMainActivity.this, DashBoardActivity.class);
                startActivity(home);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.hot_deal:
                Intent intent = new Intent(ProductMainActivity.this, HotDealActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_Product:
               /* Intent product = new Intent(ProductMainActivity.this, ProductMainActivity.class);
                startActivity(product);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);*/
                break;
            case R.id.nav_message:
                String business_number = pref_common.loadPrefString("business_number");
                String business_name = pref_common.loadPrefString("business_name");
                String business_email = pref_common.loadPrefString("business_email");
                Log.e("business_number", business_number);


                String smsNumber = "91 " + business_number; // E164 format without '+' sign
                Log.e("smsNumber", smsNumber);

                smsNumber = smsNumber.replace("+", "").replace("", "");
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello " + business_name + "!");
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net"); //phone number without "+" prefix
                sendIntent.setPackage("com.whatsapp");
                if (sendIntent.resolveActivity(ProductMainActivity.this.getPackageManager()) == null) {
                    pref_common.showAlert("WhatsApp not Installed");

                }
                startActivity(sendIntent);
                break;
            case R.id.nav_profile:
                Intent profile = new Intent(ProductMainActivity.this, ProfileActivity.class);
                startActivity(profile);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_setting:
                Intent setting = new Intent(ProductMainActivity.this, Setting.class);
                startActivity(setting);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;

            case R.id.nav_logout:
                ALERT_DIALOG_LOGOUT();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void ALERT_DIALOG_LOGOUT() {
        new AlertDialog.Builder(ProductMainActivity.this)
                .setTitle("Alert!")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ProductMainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                        pref_common.savePrefString("islogin", "2");

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)

                .show();
    }


    @Override
    public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> list) {
        Toast.makeText(ProductMainActivity.this, "Success to Purchased", Toast.LENGTH_SHORT);

    }
}
