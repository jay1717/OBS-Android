package com.mywork.jay.obs;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.BaseActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.Product.Adpter.DiscreteScrollViewOptions;
import com.securepreferences.SecurePreferences;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static android.content.ContentValues.TAG;

public class OBSAplication extends Application {

    private static OBSAplication instance;

    public static Resources resources;
    private static OBSAplication ggAplication;
    private HttpStack httpStack = null;
    private RequestQueue mRequestQueue;
    public static boolean isCheckedForDeprecation = false;
    public static boolean isAppVersionDeprecated = false;
    public static boolean isDashboardOpen = false;
    public static boolean isSplashScreenOpen = false;
    //    private BaseActivity baseActivity;
//    private CoreActivity coreActivity;
    private static SharedPreferences prefs, prefs1;
    private Pref_Common prf_comm;
    private BaseActivity baseActivity;
    private CoreActivity coreActivity;
    private static String secureKey = "";


    public static synchronized OBSAplication getGGApp() {
        return ggAplication;
    }

    private static SSLSocketFactory createSslSocketFactory() {
        TrustManager[] byPassTrustManagers = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};

        SSLContext sslContext = null;
        SSLSocketFactory sslSocketFactory = null;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, byPassTrustManagers, new SecureRandom());
            sslSocketFactory = sslContext.getSocketFactory();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            //  Log.e(TAG, StringUtils.EMPTY, e);
        } catch (KeyManagementException e) {
            //Log.e(TAG, StringUtils.EMPTY, e);
        }

        return sslSocketFactory;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        instance = this;
        DiscreteScrollViewOptions.init(this);
        ggAplication = this;
        prefs = new SecurePreferences(this, secureKey, "Pref_data.xml");
        prefs1 = new SecurePreferences(this, secureKey, "data.xml");
        prf_comm = new Pref_Common(this);


//        pref = getSharedPreferences("iMEDICA",
//                MODE_PRIVATE);

        // httpStack = new HttpStack(null, createSslSocketFactory());
        //new HurlStack(null, createSslSocketFactory());
        httpStack = new HurlStack(null, createSslSocketFactory());
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        resources = getResources();

//        useCompatVectorIfNeeded();

    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), httpStack);
        }
        return mRequestQueue;
    }

    public RequestQueue getRequestQueueCount() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), httpStack);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        try {
            getRequestQueue().add(req);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static boolean isAppVersionDeprecated() {
        return isAppVersionDeprecated;
    }

    public static boolean isDashboardOpen() {
        return isDashboardOpen;
    }

    public static boolean isSplashScreenOpen() {
        return isSplashScreenOpen;
    }

    /**
     * Fetch a welcome message from the Remote Config service, and then activate it.
     */

/*


    private void displayWelcomeMessage() {
        String welcomeMessage = mFirebaseRemoteConfig.getString(WELCOME_MESSAGE_KEY);
        if (mFirebaseRemoteConfig.getBoolean(WELCOME_MESSAGE_CAPS_KEY)) {
            mWelcomeTextView.setAllCaps(true);
        } else {
            mWelcomeTextView.setAllCaps(false);
        }
        mWelcomeTextView.setText(welcomeMessage);
    }
*/
    public String getDeviceName() {
        String device_name = "Unknown Android Device";
        try {
            device_name = Build.MANUFACTURER
                    + " " + Build.MODEL + " " + Build.VERSION.RELEASE;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return device_name;
    }

    public CoreActivity getBaseActivity() {
        return coreActivity;
    }

    public void setBaseActivity(CoreActivity coreActivity) {
        this.coreActivity = coreActivity;
    }


    public BaseActivity getBase2Activity() {
        return baseActivity;
    }

    public static SharedPreferences getSharedPreference() {
        return prefs;
    }

    public static SharedPreferences getSharedPreference1() {
        return prefs1;
    }


    public static OBSAplication getInstance() {
        return instance;
    }
}
