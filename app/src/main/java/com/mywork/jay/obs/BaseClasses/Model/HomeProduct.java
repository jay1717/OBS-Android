
package com.mywork.jay.obs.BaseClasses.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeProduct {
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("parent_id")
    @Expose
    private Object parentId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("VendorToCategory_id")
    @Expose
    private Integer vendorToCategoryId;
    @SerializedName("ios_cat_id")
    @Expose
    private Integer iosCatId;
    @SerializedName("VendorMaster_id")
    @Expose
    private Integer vendorMasterId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public Object getParentId() {
        return parentId;
    }

    public void setParentId(Object parentId) {
        this.parentId = parentId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getVendorToCategoryId() {
        return vendorToCategoryId;
    }

    public void setVendorToCategoryId(Integer vendorToCategoryId) {
        this.vendorToCategoryId = vendorToCategoryId;
    }

    public Integer getIosCatId() {
        return iosCatId;
    }

    public void setIosCatId(Integer iosCatId) {
        this.iosCatId = iosCatId;
    }

    public Integer getVendorMasterId() {
        return vendorMasterId;
    }

    public void setVendorMasterId(Integer vendorMasterId) {
        this.vendorMasterId = vendorMasterId;
    }


}
