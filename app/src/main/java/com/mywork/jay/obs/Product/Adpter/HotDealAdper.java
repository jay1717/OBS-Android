package com.mywork.jay.obs.Product.Adpter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mywork.jay.obs.Product.model.HotDeal;
import com.mywork.jay.obs.R;

import java.util.List;

/**
 * Created by jay on 7/3/18.
 */

public class HotDealAdper extends RecyclerView.Adapter<HotDealAdper.HotdealHolder> {
    private Context context;
    private List<HotDeal> hotDeals;

    public HotDealAdper(Context context, List<HotDeal> hotDeals) {
        this.context = context;
        this.hotDeals = hotDeals;

    }

    @Override
    public HotdealHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_hot_deal,parent,false);
        return new HotDealAdper.HotdealHolder(view);
    }

    @Override
    public void onBindViewHolder(HotdealHolder holder, int position) {

    }
    public void notifyData(List<HotDeal> hotDeals) {
        Log.d("notifyData ", hotDeals.size() + "");
        this.hotDeals = hotDeals;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class HotdealHolder extends RecyclerView.ViewHolder {
        private RecyclerView rv_hot_deal_product_;
        private LinearLayoutManager recyclerViewlayoutManager;

        public HotdealHolder(View itemView) {
            super(itemView);
            rv_hot_deal_product_ = (RecyclerView)itemView.findViewById(R.id.rv_hot_deal_product_);
            recyclerViewlayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
            rv_hot_deal_product_.setLayoutManager(recyclerViewlayoutManager);
            rv_hot_deal_product_.setHasFixedSize(true);

        }
    }
}
