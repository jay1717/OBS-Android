package com.mywork.jay.obs.Product.Adpter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealsDetailActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.SearchActivity;
import com.mywork.jay.obs.Product.model.Product;
import com.mywork.jay.obs.Product.model.ProductsImage;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay on 20/3/18.
 */

public class SearchAdpter extends RecyclerView.Adapter<SearchAdpter.SearchListHoder> {
    private Context context;
    List<ProductsImage> productsImages = new ArrayList<>();

    public SearchAdpter(Context context, List<ProductsImage> productsImages) {
        this.context = context;
        this.productsImages = productsImages;
    }



    @Override
    public SearchListHoder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_product_list, parent, false);
        return new SearchAdpter.SearchListHoder(view);
    }

    @Override
    public void onBindViewHolder(SearchListHoder holder, final int position) {
        Picasso.with(context).load(productsImages.get(position).getCategoryImage()).placeholder(R.drawable.watch).into(holder.img_product);

        holder.txt_product_name.setText(productsImages.get(position).getProductsName());
        holder.ll_strip_product_list_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HotDealsDetailActivity.class);
                intent.putExtra("product_name", productsImages.get(position).getProductsName());
                intent.putExtra("product_id", productsImages.get(position).getProductsId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productsImages.size();
    }

    public class SearchListHoder extends RecyclerView.ViewHolder {
        private ImageView img_product;
        private TextView txt_product_name;
        private LinearLayout ll_strip_product_list_;

        public SearchListHoder(View itemView) {
            super(itemView);
            img_product = (ImageView) itemView.findViewById(R.id.strip_product_list_img_product);
            txt_product_name = (TextView) itemView.findViewById(R.id.strip_product_list_txt_product_name);
            ll_strip_product_list_ = (LinearLayout) itemView.findViewById(R.id.ll_strip_product_list_);
        }
    }
}
