
package com.mywork.jay.obs.BaseClasses.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeModel implements  Serializable {

    @SerializedName("HotDealsHome")
    @Expose
    private List<HotDealsHome> hotDealsHome = null;
    @SerializedName("HomeCat")
    @Expose
    private List<HomeProduct> homeProducts = null;



    public List<HotDealsHome> getHotDealsHome() {
        return hotDealsHome;
    }

    public void setHotDealsHome(List<HotDealsHome> hotDealsHome) {
        this.hotDealsHome = hotDealsHome;
    }

    public List<HomeProduct> getHomeProducts() {
        return homeProducts;
    }

    public void setHomeProducts(List<HomeProduct> homeProducts) {
        this.homeProducts = homeProducts;
    }
}
