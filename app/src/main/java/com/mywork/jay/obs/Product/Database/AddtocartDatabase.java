package com.mywork.jay.obs.Product.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.mywork.jay.obs.BaseClasses.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay on 21/3/18.
 */

public class AddtocartDatabase extends SQLiteOpenHelper {


    String TAG = "AddtocartDatabase";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "addtocart";
    public static final String TABLE_PRODUCTS = "products";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_PRODUCTCATE = "productcate";
    private static final String COLUMN_PRODUCTCATE_ID = "productcate_id";
    private static final String COLUMN_PRODUCTID = "productid";
    private static final String COLUMN_PRODUCTNAME = "productname";
    private static final String COLUMN_PRODUCTDEC = "productdec";
    private static final String COLUMN_PRODUCTIMAGE = "productimage";
    private static final String COLUMN_PRODUCTPRICE = "productprice";
    private static final String COLUMN_QUANTITY = "quantity";
    private static final String COLUMN_PRODUCTVENDOR_ID = "productvendor_id";


    public AddtocartDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRODUCTS_TABLE = "CREATE    TABLE " + TABLE_PRODUCTS + "(" + COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_PRODUCTCATE + " TEXT," + COLUMN_PRODUCTCATE_ID + " TEXT," +COLUMN_PRODUCTID+ " TEXT," + COLUMN_PRODUCTNAME + " TEXT,"  + COLUMN_PRODUCTDEC + " TEXT," +
                COLUMN_PRODUCTIMAGE + " TEXT," + COLUMN_PRODUCTPRICE + " TEXT," + COLUMN_QUANTITY + " INTEGER, " + COLUMN_PRODUCTVENDOR_ID + " TEXT" + ")";
        db.execSQL(CREATE_PRODUCTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        onCreate(db);
    }

    public List<AddtoCart> listProducts() {
        String sql = "select * from " + TABLE_PRODUCTS;
        SQLiteDatabase db = this.getReadableDatabase();
        List<AddtoCart> storeProducts = new ArrayList<>();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(0);
                String cate = cursor.getString(1);
                String productid = cursor.getString(2);
                String name = cursor.getString(3);
                String cat_id = cursor.getString(4);
                Log.e("cat_id", "DataBase" + cat_id);
                String dec = cursor.getString(5);
                Log.e("dec", dec);
                String image = cursor.getString(6);
                String price = cursor.getString(7);
                String quantity = cursor.getString(8);
                String vendor_id = cursor.getString(9);
                Log.e("vendor_id", vendor_id);

                storeProducts.add(new AddtoCart(id, cate, productid, name, cat_id, dec, image, price, quantity, vendor_id));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return storeProducts;
    }

    public void addProduct(AddtoCart addtoCart) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_PRODUCTCATE, addtoCart.getProductcat());
        values.put(COLUMN_PRODUCTID, addtoCart.getProductid());
        values.put(COLUMN_PRODUCTNAME, addtoCart.getPrductname());
        values.put(COLUMN_PRODUCTCATE_ID, addtoCart.getProductcatId());
        Log.e("aadd",""+addtoCart.getProductcatId());
        values.put(COLUMN_PRODUCTDEC, addtoCart.getProductcatDec());
        values.put(COLUMN_PRODUCTIMAGE, addtoCart.getProductimage());
        values.put(COLUMN_PRODUCTPRICE, addtoCart.getProductprice());
        values.put(COLUMN_QUANTITY, addtoCart.getProductquantity());
        values.put(COLUMN_PRODUCTVENDOR_ID, addtoCart.getProductvendor_id());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_PRODUCTS, null, values);
    }

    public void updateProduct(AddtoCart addtoCart) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_PRODUCTCATE, addtoCart.getProductcat());
        values.put(COLUMN_PRODUCTID, addtoCart.getProductid());
        values.put(COLUMN_PRODUCTNAME, addtoCart.getPrductname());
        values.put(COLUMN_PRODUCTCATE_ID, addtoCart.getProductcatId());
        values.put(COLUMN_PRODUCTDEC, addtoCart.getProductcatDec());
        values.put(COLUMN_PRODUCTIMAGE, addtoCart.getProductimage());
        values.put(COLUMN_PRODUCTPRICE, addtoCart.getProductprice());
        values.put(COLUMN_QUANTITY, addtoCart.getProductquantity());
        values.put(COLUMN_PRODUCTVENDOR_ID, addtoCart.getProductvendor_id());
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_PRODUCTS, values, COLUMN_ID + "    = ?", new String[]{String.valueOf(addtoCart.getId())});
    }


    public AddtoCart findProduct(String productid, String name, String dec, String cat_id, String cate, String image, String price, String quantity, String vendor_id) {
        String query = "Select * FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCTNAME + " = " + "name";
        SQLiteDatabase db = this.getWritableDatabase();
        AddtoCart mProduct = null;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String id = cursor.getString(0);
            cate = cursor.getString(1);
            productid = cursor.getString(2);
            name = cursor.getString(3);
            cat_id = cursor.getString(4);
            dec = cursor.getString(5);
            image = cursor.getString(6);
            price = cursor.getString(7);
            quantity = cursor.getString(8);
            vendor_id = cursor.getString(9);
            mProduct = new AddtoCart(id, cate, productid, name, cat_id, dec, image, price, quantity, vendor_id);
        }
        cursor.close();
        return mProduct;
    }

    public void deleteProduct(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRODUCTS, COLUMN_ID + "    = ?", new String[]{String.valueOf(id)});
    }
    public void deleteAllProduct(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_PRODUCTS);
        db.close();


    }
}
