
package com.mywork.jay.obs.Product.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishListModel {

    @SerializedName("WishLists")
    @Expose
    private List<WishList> wishLists = null;

    public List<WishList> getWishLists() {
        return wishLists;
    }

    public void setWishLists(List<WishList> wishLists) {
        this.wishLists = wishLists;
    }

}
