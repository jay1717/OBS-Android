package com.mywork.jay.obs.Product.Navigation.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.DashBoardActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.LoginActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.Product.Adpter.CartListAdpter;
import com.mywork.jay.obs.Product.Adpter.ProductAdpter;
import com.mywork.jay.obs.Product.Database.AddtoCart;
import com.mywork.jay.obs.Product.Database.AddtocartDatabase;
import com.mywork.jay.obs.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartActivity extends CoreActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView cart_list;
    private LinearLayoutManager recyclerViewlayoutManager;
    private CartListAdpter cartListAdpter;
    public DrawerLayout drawer;
    public NavigationView navigationView;
    private ImageView imgMenu;
    private TextView txtTitle, txt_nodata;
    AddtocartDatabase addtocartDatabase;
    private TextView txt_cart_inquiry;
    private List<AddtoCart> addtoCarts = new ArrayList<>();
    private Pref_Common pref_common;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_cart);
        addtocartDatabase = new AddtocartDatabase(this);
        addtoCarts = addtocartDatabase.listProducts();
        pref_common = new Pref_Common(CartActivity.this);
        setIds();
        setAdpter();
        setClickEvents();
        setSliderDashboard(R.menu.navigation_drawer);

        for (int p = 0; p < addtoCarts.size(); p++) {
            try {
                String price =addtoCarts.get(p).getProductprice();
                Log.e("ProductId CartActivity ", "" + addtoCarts.get(p).getProductid());
                Log.e("productcat CartActivity ", "" + addtoCarts.get(p).getProductcat());
                Log.e("prductname CartActivity ", "" + addtoCarts.get(p).getPrductname());
                Log.e("productimage CartActivity ", "" + addtoCarts.get(p).getProductimage());
                Log.e("productprice CartActivity ", "" + price);
                Log.e("productquantity CartActivity ", "" + addtoCarts.get(p).getProductquantity());
                Log.e("productcatId CartActivity ", "" + addtoCarts.get(p).getProductcatId());
                Log.e("productcatDec CartActivity ", "" + addtoCarts.get(p).getProductcatDec());
                Log.e("productvendor_id CartActivity ", "" + addtoCarts.get(p).getProductvendor_id());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


        }

    }

    private void setIds() {
        try {
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            cart_list = (RecyclerView) findViewById(R.id.rv_activity_cart_list);
            imgMenu = (ImageView) findViewById(R.id.imgMenu);
            txtTitle = (TextView) findViewById(R.id.txtTitle);
            txt_nodata = (TextView) findViewById(R.id.txt_nodata);
            txt_cart_inquiry = (TextView) findViewById(R.id.txt_cart_inquiry);
            setValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickEvents() {
        try {
            imgMenu.setOnClickListener(this);
            txt_cart_inquiry.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValue() {
        try {
            txtTitle.setText("Cart");
            assert navigationView != null;
            navigationView.setNavigationItemSelectedListener(this);
            setNavigationView(navigationView);
            setDrawerLayout(drawer);
            SetImageMenuShow(imgMenu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdpter() {
        try {
            recyclerViewlayoutManager = new LinearLayoutManager(CartActivity.this, LinearLayoutManager.VERTICAL, false);
            cart_list.setLayoutManager(recyclerViewlayoutManager);
            cart_list.setHasFixedSize(true);
            if (addtoCarts.isEmpty()) {
                txt_nodata.setVisibility(View.VISIBLE);
                cart_list.setVisibility(View.GONE);
            } else {
                txt_nodata.setVisibility(View.GONE);
                cart_list.setVisibility(View.VISIBLE);
                cartListAdpter = new CartListAdpter(CartActivity.this, addtoCarts);
                cart_list.setAdapter(cartListAdpter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_Home:
                Intent home = new Intent(CartActivity.this, DashBoardActivity.class);
                startActivity(home);
                finish();
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.hot_deal:
                Intent intent = new Intent(CartActivity.this, HotDealActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_Product:
                Intent product = new Intent(CartActivity.this, ProductMainActivity.class);
                startActivity(product);
                finish();
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_message:
                String business_number = pref_common.loadPrefString("business_number");
                String business_name = pref_common.loadPrefString("business_name");
                String business_email = pref_common.loadPrefString("business_email");
                Log.e("business_number",business_number);


                String smsNumber = "91 " +business_number; // E164 format without '+' sign
                Log.e("smsNumber",smsNumber);

                smsNumber = smsNumber.replace("+", "").replace("", "");
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello " + business_name + "!");
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net"); //phone number without "+" prefix
                sendIntent.setPackage("com.whatsapp");
                if (sendIntent.resolveActivity(CartActivity.this.getPackageManager()) == null) {
                    pref_common.showAlert("WhatsApp not Installed");

                }
                startActivity(sendIntent);
                break;
            case R.id.nav_profile:
                Intent profile = new Intent(CartActivity.this, ProfileActivity.class);
                startActivity(profile);
                finish();
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_setting:
                Intent setting = new Intent(CartActivity.this, Setting.class);
                startActivity(setting);
                finish();
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;

            case R.id.nav_logout:
                ALERT_DIALOG_LOGOUT();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void ALERT_DIALOG_LOGOUT() {
        new AlertDialog.Builder(CartActivity.this)
                .setTitle("Alert!")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(CartActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                        pref_common.savePrefString("islogin", "2");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)

                .show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgMenu:
                MenuClick();
                break;
            case R.id.txt_cart_inquiry:
                popupAddday();
                break;
        }
    }

    public void popupAddday() {

        final Dialog dialog;
        dialog = new Dialog(CartActivity.this, R.style.PauseDialog);

        /** initialize object for popup **/
        final ImageView imgClose, imgDone, imgMenu;
        final EditText edt_inquary;
        final TextView txtTitle;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        dialog.setContentView(R.layout.add_title_enquiry);
        dialog.setCanceledOnTouchOutside(false);
        wmlp.gravity = Gravity.CENTER;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        /** bind view for popup**/
        imgClose = (ImageView) dialog.findViewById(R.id.imgclose);
        imgDone = (ImageView) dialog.findViewById(R.id.imgDone);
        imgMenu = (ImageView) dialog.findViewById(R.id.imgMenu);
        imgDone.setVisibility(View.VISIBLE);
        imgClose.setVisibility(View.VISIBLE);
        imgMenu.setVisibility(View.GONE);
        edt_inquary = (EditText) dialog.findViewById(R.id.edt_inquary);
        txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
        txtTitle.setText("Inquaity");

        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inquary = edt_inquary.getText().toString();
                if (inquary.isEmpty()) {
                    pref_common.showAlert("Inquiry can not be blank");
                } else {
                    SET_WEB_CALL_INQUIRY(inquary);
                    dialog.dismiss();
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void SET_WEB_CALL_INQUIRY(String inquary) {
        JSONObject object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {

            for (int i = 0; i < addtoCarts.size(); i++) {
                JSONObject obj = new JSONObject();
                obj.put("Inquiry_title", inquary);
                obj.put("Inquiry_description", addtoCarts.get(i).getProductcatDec());
                obj.put("Product_id", addtoCarts.get(i).getPrductname());
                obj.put("Vendor_id", addtoCarts.get(i).getProductvendor_id());
                obj.put("category_id", addtoCarts.get(i).getProductid());
                obj.put("quantity", addtoCarts.get(i).getProductquantity());
                obj.put("price", addtoCarts.get(i).getProductprice());
                jsonArray.put(i, obj);
            }
            object.put("inquiry", jsonArray);
            Log.e("inquiry : ", " " + object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String inquiry_url = BaseApi.postObsAPI(BaseApi.POST_ENQUIRY);

        new OBSRequest(CartActivity.this, inquiry_url, OBSRequest.METHOD_POST, true, object, new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                addtocartDatabase.deleteAllProduct();
                ((Activity)CartActivity.this).finish();
                startActivity(((Activity) CartActivity.this).getIntent());
            }

            @Override
            public void onError(String errorData, int statusCode) {
                Log.e("errorData", "" + errorData.toString());
            }
        }).makeJsonObjectRequest();
    }


}
