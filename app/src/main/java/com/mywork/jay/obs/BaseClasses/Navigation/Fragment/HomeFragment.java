package com.mywork.jay.obs.BaseClasses.Navigation.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mywork.jay.obs.BaseClasses.Adpter.FirstProduct;
import com.mywork.jay.obs.BaseClasses.Adpter.ImagePagerAdapter;
import com.mywork.jay.obs.BaseClasses.Adpter.SecondHomeProduct;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Model.HomeModel;
import com.mywork.jay.obs.BaseClasses.Model.HomeProduct;
import com.mywork.jay.obs.BaseClasses.Model.HotDealsHome;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreFragment;
import com.mywork.jay.obs.BaseClasses.com.antonyt.infiniteviewpager.library.InfiniteViewPager;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.SearchActivity;
import com.mywork.jay.obs.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by jay on 1/3/18.
 */

public class HomeFragment extends CoreFragment implements  SwipeRefreshLayout.OnRefreshListener{

    private InfiniteViewPager home_fragment_vp_bannerlist;
    private ImagePagerAdapter imagePagerAdapter;
    private RecyclerView first_product, Second_product;
    private LinearLayoutManager recyclerViewlayoutManager;
    private FirstProduct madapter;
    private SwipeRefreshLayout swiperefresh;
    private SecondHomeProduct adapter;
    private LinearLayout ll_send_search;
    private TextView txt_hot_deal_message, txt_see_all_hot_dea, no_data, no_data_1, no_data_2;
    private List<HotDealsHome> hotDeals = new ArrayList<>();
    private List<HomeProduct> homeProducts = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        setIds(view);
        setClickEvents();
        setSliderMenu(R.menu.navigation_drawer);
        GET_HOME_LIST();
        return view;
    }

    private void setIds(View view) {
        try {
            home_fragment_vp_bannerlist = (InfiniteViewPager) view.findViewById(R.id.home_fragment_vp_bannerlist);
            first_product = (RecyclerView) view.findViewById(R.id.home_fragment_first_product);
            swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
            Second_product = (RecyclerView) view.findViewById(R.id.home_fragment_Second_product);
            txt_hot_deal_message = (TextView) view.findViewById(R.id.txt_hot_deal_message);
            txt_see_all_hot_dea = (TextView) view.findViewById(R.id.txt_see_all_hot_dea);
            ll_send_search = (LinearLayout) view.findViewById(R.id.ll_send_search);
            no_data = (TextView) view.findViewById(R.id.no_data);
            no_data_2 = (TextView) view.findViewById(R.id.no_data_2);
            no_data_1 = (TextView) view.findViewById(R.id.no_data_1);
            swiperefresh.setOnRefreshListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickEvents() {
        txt_see_all_hot_dea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HotDealActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });
        ll_send_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });
    }

    private void GET_HOME_LIST() {
        String home_url = BaseApi.postObsAPI(BaseApi.GET_HOME);
        Log.e("home_url", home_url);

        Map<String, String> param = new HashMap<>();

        new OBSRequest(getActivity(), home_url, OBSRequest.METHOD_GET, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                com.mywork.jay.obs.BaseClasses.util.Log.e("response", response);
                HomeModel homeModel = new Gson().fromJson(response, HomeModel.class);
                if (homeModel != null) {
                    hotDeals = homeModel.getHotDealsHome();
                    homeProducts = homeModel.getHomeProducts();
                    Log.e("homeProducts", "" + hotDeals.size());
                    setAdper();
                    if (hotDeals.size() > 0) {
                        first_product.setVisibility(View.VISIBLE);
                        no_data_1.setVisibility(View.GONE);
                        setFirstProduct();
                    } else {
                        first_product.setVisibility(View.GONE);
                        no_data_1.setVisibility(View.VISIBLE);
                    }
                    if (homeProducts.size() > 0) {
                        Second_product.setVisibility(View.VISIBLE);
                        no_data_2.setVisibility(View.GONE);
                        recyclerViewlayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        Second_product.setLayoutManager(recyclerViewlayoutManager);
                        Second_product.setHasFixedSize(true);
                        adapter = new SecondHomeProduct(getActivity(), homeModel.getHomeProducts());
                        Second_product.setAdapter(adapter);
                    } else {
                        Second_product.setVisibility(View.GONE);
                        no_data_2.setVisibility(View.VISIBLE);
                    }

                    for (int p = 0; p < hotDeals.size(); p++) {
                        txt_hot_deal_message.setText(hotDeals.get(p).getHotDealsDescription());
                    }
                }
            }

            @Override
            public void onError(String errorData, int statusCode) {
                Log.e("errorData", errorData.toString());
            }
        }).makeRegisterJsonObjectRequest(param);
    }

    private void setAdper() {
        try {
            ArrayList<String> imageList = new ArrayList<>();
            if (hotDeals != null && hotDeals.size() > 0) {
                for (int j = 0; j < hotDeals.size(); j++) {
                    imageList.add(hotDeals.get(j).getHotDealsImage());
                }
                imagePagerAdapter = new ImagePagerAdapter(getActivity());
                home_fragment_vp_bannerlist.setAdapter(imagePagerAdapter);
                imagePagerAdapter.setDataList(imageList);
                home_fragment_vp_bannerlist.setAutoScrollTime(3000);
                home_fragment_vp_bannerlist.startAutoScroll();
            }else {
                if (imageList.size() > 0) {
                    home_fragment_vp_bannerlist.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.GONE);
                    imagePagerAdapter = new ImagePagerAdapter(getActivity());
                    home_fragment_vp_bannerlist.setAdapter(imagePagerAdapter);
                    imagePagerAdapter.setDataList(imageList);
                    home_fragment_vp_bannerlist.setAutoScrollTime(3000);
                    home_fragment_vp_bannerlist.startAutoScroll();
                } else {
                    home_fragment_vp_bannerlist.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFirstProduct() {
        try {
            recyclerViewlayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            first_product.setLayoutManager(recyclerViewlayoutManager);
            first_product.setHasFixedSize(true);
            madapter = new FirstProduct(getActivity(), hotDeals);
            first_product.setAdapter(madapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onRefresh() {
        swiperefresh.setRefreshing(false);
        GET_HOME_LIST();
    }
}

