package com.mywork.jay.obs.Product.Adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mywork.jay.obs.Product.Navigation.Activity.ProfileActivity;
import com.mywork.jay.obs.Product.model.WishList;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jay on 6/3/18.
 */

public class ProfileSaveAdpter extends RecyclerView.Adapter<ProfileSaveAdpter.ProfileSaveHolder> {
    private Context context;
    private List<WishList> wishLists;
    public ProfileSaveAdpter(Context context, List<WishList> wishLists) {
        this.context = context;
        this.wishLists = wishLists;
    }

    @Override
    public ProfileSaveHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_profile_save,parent,false);
        return new ProfileSaveAdpter.ProfileSaveHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfileSaveHolder holder, int position) {

        Picasso.with(context).load(wishLists.get(position).getProductImage()).placeholder(R.drawable.watch).into(holder._img_wish);
        holder._txt_wish.setText(wishLists.get(position).getProductsName());

    }

    @Override
    public int getItemCount() {
        return wishLists.size();
    }

    public class ProfileSaveHolder extends RecyclerView.ViewHolder {
        private ImageView _img_wish;
        private TextView _txt_wish;
        public ProfileSaveHolder(View itemView) {
            super(itemView);
            _img_wish = (ImageView)itemView.findViewById(R.id.row_item_profile_save_img_wish);
            _txt_wish = (TextView) itemView.findViewById(R.id.row_item_profile_save_txt_wish);
        }
    }
}
