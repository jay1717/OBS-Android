package com.mywork.jay.obs.Product.Navigation.Activity;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.model.HotDeal;
import com.mywork.jay.obs.Product.model.HotDealModel;
import com.mywork.jay.obs.Product.Adpter.HotDealProductAdper;
import com.mywork.jay.obs.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HotDealActivity extends CoreActivity implements View.OnClickListener {

    private RecyclerView rv_hoat_deals;
    private GridLayoutManager recyclerViewlayoutManager;
    private ImageView imgMenu,img_txt_Back;
    private TextView txtTitle;
    private List<HotDeal> hotDeals = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_deal);

        setIdS();
        setValue();
        setClickEvent();
        setLayoutmanager();
        GET_HOT_DEAL_DATA();
    }
    private void setIdS(){
        try{
            rv_hoat_deals = (RecyclerView)findViewById(R.id.rv_hoat_deals);
            imgMenu = (ImageView)findViewById(R.id.imgMenu);
            img_txt_Back = (ImageView)findViewById(R.id.img_txt_Back);
            txtTitle= (TextView)findViewById(R.id.txtTitle);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setValue(){
        try {
            txtTitle.setText("Today's Deals");
            imgMenu.setVisibility(View.GONE);
            img_txt_Back.setVisibility(View.VISIBLE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setClickEvent(){
        try {
            img_txt_Back.setOnClickListener(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setLayoutmanager(){
        try {

            recyclerViewlayoutManager = new GridLayoutManager(HotDealActivity.this,LinearLayoutManager.VERTICAL);
            recyclerViewlayoutManager.setSpanCount(2);
            rv_hoat_deals.setLayoutManager(recyclerViewlayoutManager);
            rv_hoat_deals.setHasFixedSize(true);
           /* HotDealProductAdper madapter = new HotDealProductAdper(HotDealActivity.this,hotDeals);
            rv_hoat_deals.setAdapter(madapter);*/


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_txt_Back:
                finish();
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                break;
        }
    }

    private void GET_HOT_DEAL_DATA(){
        String hotdeal_url = BaseApi.postObsAPI(BaseApi.POST_HOT_DEAL);


        Map<String, String> param = new HashMap<>();

        new OBSRequest(HotDealActivity.this, hotdeal_url, OBSRequest.METHOD_GET, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {

                HotDealModel hotDealModel = new Gson().fromJson(response.toString(),HotDealModel.class);
                hotDeals = hotDealModel.getHotDeals();
                HotDealProductAdper madapter = new HotDealProductAdper(HotDealActivity.this,hotDeals);
                rv_hoat_deals.setAdapter(madapter);
            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);
    }
}
