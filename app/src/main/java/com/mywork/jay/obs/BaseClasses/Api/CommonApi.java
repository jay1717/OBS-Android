package com.mywork.jay.obs.BaseClasses.Api;

/**
 * Created by jay on 8/1/18.
 */

public class CommonApi {

    public static final String BASE_DOMAIN = ".com/";
    public static final String BASE_NAME = "gft/";
    public static final boolean IS_SECURE = true;
    public static final boolean APP_LOG = true;

//    public static final String BASE_TRAINING_PROGRAM = getOBSUrl(false,"obs",BASE_DOMAIN);//"http://api.sulphurtechnologies.com";
    public static final String BASE_TRAINING_PROGRAM = getOBSUrl(false,"obsapi-ios",BASE_DOMAIN);//"http://api.sulphurtechnologies.com";

    public static String getObsAPI(String endPoint) {
        return BASE_TRAINING_PROGRAM +  endPoint;
    }
    public static String getOBSUrl(boolean isSecure, String SubDomain, String HostDomain) {
        if (isSecure) {
            return "http://" + SubDomain + ".sulphurtechnologies" + HostDomain + "/obs-api/public/api/";
        } else {
            return "http://" + SubDomain + ".sulphurtechnologies" + HostDomain + "obs-api/public/api/";
        }
    }

}
