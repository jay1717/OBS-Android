package com.mywork.jay.obs.Product.Navigation.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.DashBoardActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.LoginActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.Product.Adpter.ProfileSaveAdpter;
import com.mywork.jay.obs.Product.model.WishList;
import com.mywork.jay.obs.Product.model.WishListModel;
import com.mywork.jay.obs.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends CoreActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView rv_save_product;
    private LinearLayoutManager recyclerViewlayoutManager;
    private ProfileSaveAdpter productAdpter;
    public DrawerLayout drawer;
    public NavigationView navigationView;
    private TextView txtTitle, txt_no_wish_list, txt_name, txt_mobile, txt_email;
    private ImageView imgEdit, imgMenu;
    private Pref_Common pref_common;
    private List<WishList> wishLists = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_profile_);
        pref_common = new Pref_Common(ProfileActivity.this);
        setIds();
        setAdptr();
        setValue();
        setClickEvents();
        initBottomTab();
        setBottomTabSelected(3);
        setSliderDashboard(R.menu.navigation_drawer);
        GET_PROFILE_DETAIL();
    }

    private void setIds() {
        try {
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            rv_save_product = (RecyclerView) findViewById(R.id.activity_parofil_rv_save_product);
            txtTitle = (TextView) findViewById(R.id.txtTitle);
            txt_email = (TextView) findViewById(R.id.txt_email);
            txt_mobile = (TextView) findViewById(R.id.txt_mobile);
            txt_name = (TextView) findViewById(R.id.txt_name);
            txt_no_wish_list = (TextView) findViewById(R.id.txt_no_wish_list);
            imgEdit = (ImageView) findViewById(R.id.imgEdit);
            imgMenu = (ImageView) findViewById(R.id.imgMenu);
            nav_setValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickEvents() {
        try {
            imgMenu.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdptr() {
        try {
            recyclerViewlayoutManager = new LinearLayoutManager(ProfileActivity.this, LinearLayoutManager.HORIZONTAL, false);
            rv_save_product.setLayoutManager(recyclerViewlayoutManager);
            rv_save_product.setHasFixedSize(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void nav_setValue() {
        try {
            assert navigationView != null;
            navigationView.setNavigationItemSelectedListener(this);
            setNavigationView(navigationView);
            setDrawerLayout(drawer);
            SetImageMenuShow(imgMenu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValue() {
        try {
            imgEdit.setVisibility(View.GONE);
            txtTitle.setText("Profile");
            txtTitle.setTextColor(getResources().getColor(R.color.colorBlack));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgMenu:
                MenuClick();
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_Home:
                Intent home = new Intent(ProfileActivity.this, DashBoardActivity.class);
                startActivity(home);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.hot_deal:
                Intent intent = new Intent(ProfileActivity.this, HotDealActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_Product:
                Intent product = new Intent(ProfileActivity.this, ProductMainActivity.class);
                startActivity(product);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_message:
                String business_number = pref_common.loadPrefString("business_number");
                String business_name = pref_common.loadPrefString("business_name");
                String business_email = pref_common.loadPrefString("business_email");
                Log.e("business_number",business_number);


                String smsNumber = "91 " +business_number; // E164 format without '+' sign
                Log.e("smsNumber",smsNumber);

                smsNumber = smsNumber.replace("+", "").replace("", "");
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello " + business_name + "!");
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net"); //phone number without "+" prefix
                sendIntent.setPackage("com.whatsapp");
                if (sendIntent.resolveActivity(ProfileActivity.this.getPackageManager()) == null) {
                    pref_common.showAlert("WhatsApp not Installed");

                }
                startActivity(sendIntent);
                break;
            case R.id.nav_profile:
               /* Intent profile = new Intent(ProfileActivity.this, ProfileActivity.class);
                startActivity(profile);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);*/
                break;
            case R.id.nav_setting:
                Intent setting = new Intent(ProfileActivity.this, Setting.class);
                startActivity(setting);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;

            case R.id.nav_logout:
                ALERT_DIALOG_LOGOUT();

                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void ALERT_DIALOG_LOGOUT() {
        new AlertDialog.Builder(ProfileActivity.this)
                .setTitle("Alert!")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)

                .show();
    }

    private void WISH_LIST_PRODUCT() {
        String add_widh = BaseApi.postObsAPI(BaseApi.ADD_WISH_LIST);

        Map<String, String> param = new HashMap<>();

        new OBSRequest(ProfileActivity.this, add_widh, OBSRequest.METHOD_POST, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                WishListModel wishListModel = new Gson().fromJson(response.toString(), WishListModel.class);
                wishLists = wishListModel.getWishLists();
                if (wishLists.size() == 0 || wishLists.equals(null) || wishLists.isEmpty()) {
                    txt_no_wish_list.setVisibility(View.VISIBLE);
                    rv_save_product.setVisibility(View.GONE);
                } else {
                    txt_no_wish_list.setVisibility(View.GONE);
                    rv_save_product.setVisibility(View.VISIBLE);
                    productAdpter = new ProfileSaveAdpter(ProfileActivity.this, wishLists);
                    rv_save_product.setAdapter(productAdpter);
                }

            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);

    }

    private void GET_PROFILE_DETAIL() {
        String detail_url = BaseApi.postObsAPI(BaseApi.POST_PROFILE_DETAILS);

        Map<String, String> param = new HashMap<>();

        new OBSRequest(ProfileActivity.this, detail_url, OBSRequest.METHOD_POST, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {

                try {
                    JSONObject object = new JSONObject(response.toString());
                    JSONObject jsonObject = object.getJSONObject("success");

                        String name = jsonObject.getString("name");
                        String email = jsonObject.getString("email");
                        String mobile = jsonObject.getString("mobile_number");
                        txt_name.setText(name);
                        txt_mobile.setText(mobile);
                        txt_email.setText(email);
                        WISH_LIST_PRODUCT();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);
    }
}


