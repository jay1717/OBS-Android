package com.mywork.jay.obs.Product.Adpter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mywork.jay.obs.BaseClasses.Adpter.FirstProduct;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealsDetailActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.ProductMainActivity;
import com.mywork.jay.obs.Product.model.Product;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bumptech.glide.load.DecodeFormat.PREFER_ARGB_8888;

/**
 * Created by jay on 20/3/18.
 */

class ProductListAdpter extends RecyclerView.Adapter<ProductListAdpter.ProductListHoder> {
    private Context context;
    List<Product> productList = new ArrayList<>();
    private Pref_Common pref_common;
    BillingClient billingClient;

    public ProductListAdpter(Context context, List<Product> productList, BillingClient billingClient) {
        this.context = context;
        this.productList = productList;
        this.billingClient = billingClient;
        pref_common = new Pref_Common(context);
    }

    @Override
    public ProductListHoder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_product_list,parent,false);
        return new ProductListAdpter.ProductListHoder(view);
    }

    @Override
    public void onBindViewHolder(final ProductListHoder holder, final int position) {
        Glide.with(context)
                .load(productList.get(position).getProductImage()).asBitmap().thumbnail(0.1f)
                .placeholder(R.drawable.placeholder_1140x350)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL).format(PREFER_ARGB_8888)
                .into(holder.img_product);

        if (productList.get(position).getProductsName() == null || productList.get(position).getProductsName().isEmpty()){
            holder.txt_product_name.setText("-");
        }else {
            holder.txt_product_name.setText(productList.get(position).getProductsName());
        }

        if (productList.get(position).getIsWished() == true) {
            holder.img_wish_hart.setImageResource(R.drawable.hart_fill);
        }else  if (productList.get(position).getIsWished() == false){
            holder.img_wish_hart.setImageResource(R.drawable.hart_unfill);
        }
        Log.e("name",productList.get(position).getProductsName());
        holder.ll_strip_product_list_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Ready(position);
            }
        });

        holder.img_wish_hart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ADD_WISH_LIST_PRODUCT(String.valueOf(productList.get(position).getProductsId()),holder,position);
            }
        });


        if (productList.get(position).getProductsPrice().isEmpty()){
            holder.product_price.setText("");
        }else {
            holder.product_price.setText("MYR:-  "+productList.get(position).getProductsPrice());
        }

    }

    private void Ready( final int pos){

        if (billingClient.isReady()){

            SkuDetailsParams params = SkuDetailsParams.newBuilder().setSkusList(Arrays.asList("d_t_p","h_t_p","s_t_p","k_t_p")).setType(BillingClient.SkuType.INAPP).build();

            billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK){
                        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetailsList.get(pos)).build();
                        billingClient.launchBillingFlow((Activity) context, billingFlowParams);
                    }else if(billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED){
                        Toast.makeText(context, "This Product is already purchased", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(context, "Some thing went Wrong", Toast.LENGTH_SHORT).show();
                    }
                }


            });
        }
    }
    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductListHoder extends RecyclerView.ViewHolder {
        private ImageView img_product,img_wish_hart;
        private TextView txt_product_name,product_price;
        private LinearLayout ll_strip_product_list_;
        private ProgressBar circular_progress;

        public ProductListHoder(View itemView) {
            super(itemView);
            img_product = (ImageView)itemView.findViewById(R.id.strip_product_list_img_product);
            img_wish_hart = (ImageView)itemView.findViewById(R.id.img_wish_hart);
            txt_product_name = (TextView)itemView.findViewById(R.id.strip_product_list_txt_product_name);
            product_price = (TextView)itemView.findViewById(R.id.txt_strip_product_list_price);
            ll_strip_product_list_  =(LinearLayout)itemView.findViewById(R.id.ll_strip_product_list_);
            circular_progress = (ProgressBar)itemView.findViewById(R.id.circular_progress);

        }
    }

    private void ADD_WISH_LIST_PRODUCT(String product_id, final ProductListHoder holder, final int position) {
        String add_widh = BaseApi.postObsAPI(BaseApi.ADD_WISH_LIST);

        Map<String, String> param = new HashMap<>();
        param.put("product_id", String.valueOf(product_id));
        new OBSRequest(context, add_widh, OBSRequest.METHOD_PATCH, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getBoolean("status") == true) {
                        Toast.makeText(context, "Add to wish list", Toast.LENGTH_SHORT).show();
                        holder.img_wish_hart.setImageResource(R.drawable.hart_fill);

                    } else if (jsonObject.getBoolean("status") == false) {
                        holder.img_wish_hart.setImageResource(R.drawable.hart_unfill);
                        Toast.makeText(context, "Remove from wish list", Toast.LENGTH_SHORT).show();
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String errorData, int statusCode) {
            }
        }).makeRegisterJsonObjectRequest(param);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
