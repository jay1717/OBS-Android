package com.mywork.jay.obs.BaseClasses.Navigation.Core;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.mywork.jay.obs.OBSAplication;

/**
 * Created by rgi-5 on 17/11/16.
 */

public class CoreFragment extends Fragment implements View.OnClickListener{


    private boolean isBackShow = false;
    private long mLastClickTime = 0;
    public static final long MAX_CLICK_INTERVAL = 1000;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

//        getBaseActivity().hideSoftKeyboard();
//        getBase2Activity().hideSoftKeyboard();
        hideSoftKeyboard();
    }

    public CoreActivity getBaseActivity(){
        if(getActivity() != null){
            return (CoreActivity) getActivity();
        }
        else{
            return OBSAplication.getGGApp().getBaseActivity();
        }
    }

    public BaseActivity getBase2Activity(){
        if(getActivity() != null){
            return (BaseActivity) getActivity();
        }
        else{
            return OBSAplication.getGGApp().getBase2Activity();
        }
    }

    public void hideSoftKeyboard() {
        try{
            if(getActivity().getCurrentFocus()!=null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setSliderMenu(int menuId){
        getBaseActivity().setSliderMenu(menuId);
    }


    public boolean isBackShow(){
        return isBackShow;
    }

    public void setBackShow(boolean isBackShow){
        this.isBackShow = isBackShow;
    }

    public LinearSnapHelper setFix(){
        LinearSnapHelper snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                View centerView = findSnapView(layoutManager);
                if (centerView == null)
                    return RecyclerView.NO_POSITION;

                int position = layoutManager.getPosition(centerView);
                int targetPosition = -1;
                if (layoutManager.canScrollHorizontally()) {
                    if (velocityX < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                if (layoutManager.canScrollVertically()) {
                    if (velocityY < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                final int firstItem = 0;
                final int lastItem = layoutManager.getItemCount() - 1;
                targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                return targetPosition;
            }
        };
//        snapHelper.attachToRecyclerView(recyclerView);
        return snapHelper;
    }

    public void setHelperr(RecyclerView recyclerView){
        LinearSnapHelper snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                View centerView = findSnapView(layoutManager);
                if (centerView == null)
                    return RecyclerView.NO_POSITION;

                int position = layoutManager.getPosition(centerView);
                int targetPosition = -1;
                if (layoutManager.canScrollHorizontally()) {
                    if (velocityX < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                if (layoutManager.canScrollVertically()) {
                    if (velocityY < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                final int firstItem = 0;
                final int lastItem = layoutManager.getItemCount() - 1;
                targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                return targetPosition;
            }
        };
        snapHelper.attachToRecyclerView(recyclerView);
    }


    @Override
    public void onClick(View v) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < MAX_CLICK_INTERVAL) {
            return;
        }
    }
}
