package com.mywork.jay.obs.Product.Adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mywork.jay.obs.Product.Navigation.Activity.HotDealsDetailActivity;
import com.mywork.jay.obs.Product.model.ProductImage;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay on 7/3/18.
 */

public class HotDealsDetalAdpter extends RecyclerView.Adapter<HotDealsDetalAdpter.HotDealiDetalisHolder> {

    private Context context;
    private List<ProductImage> productImages;


    public HotDealsDetalAdpter(Context context,List<ProductImage> productImages) {
        this.context = context;
        this.productImages = productImages;
    }


    @Override
    public HotDealiDetalisHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.strip_gallry_hot_deal_detal, parent, false);
        return new HotDealsDetalAdpter.HotDealiDetalisHolder(v);
    }

    @Override
    public void onBindViewHolder(HotDealiDetalisHolder holder, int position) {

        Picasso.with(context).load(productImages.get(position).getProductImage()).placeholder(R.drawable.watch).into(holder.product_image);

    }

    @Override
    public int getItemCount() {
        return productImages.size();
    }

    public class HotDealiDetalisHolder extends RecyclerView.ViewHolder {
        private ImageView product_image;
        public HotDealiDetalisHolder(View itemView) {
            super(itemView);
            product_image = (ImageView)itemView.findViewById(R.id.strip_gallry_hot_deal_detal_product_image);
        }
    }
}
