package com.mywork.jay.obs.BaseClasses.util.permission;


import com.mywork.jay.obs.BaseClasses.Api.CommonApi;

/**
 * Created by rgi-5 on 19/5/17.
 */

public class Log {
    static final boolean LOG = CommonApi.APP_LOG;

    public static void i(String tag, String string) {
        if (LOG) android.util.Log.i("GFT-" + tag, string);
    }

    public static void e(String tag, String string) {
        if (LOG) android.util.Log.e("GFT-" + tag, string);
    }

    public static void d(String tag, String string) {
        if (LOG) android.util.Log.d("GFT-" + tag, string);
    }

    public static void v(String tag, String string) {
        if (LOG) android.util.Log.v("GFT-" + tag, string);
    }

    public static void w(String tag, String string) {
        if (LOG) android.util.Log.w("GFT-" + tag, string);
    }
}



