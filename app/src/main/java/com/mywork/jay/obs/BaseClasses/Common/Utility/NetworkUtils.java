package com.mywork.jay.obs.BaseClasses.Common.Utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;


/**
 * Created by rgi-5 on 15/11/16.
 */

public class NetworkUtils {

    public static boolean isInternetConnected(Context context) {
        boolean isConnected = false;
        try {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null) {
                if (networkInfo.isConnected()) {
                    isConnected = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnected;
    }

    public static boolean isNetworkAvailable(Context context) {
        boolean isConnect = isInternetConnected(context);
        if (!isConnect) {
            Toast.makeText(context, "No Network",Toast.LENGTH_SHORT).show();
//            com.globalgarner.BaseClasses.util.Utils.showTitleAlert(context, context.getResources().getString(R.string.str_internet_alert_title), context.getResources().getString(R.string.str_internet_alert_msg));
        }
        return isConnect;
    }
    public static boolean isNetworkAvailableProcess(Context context) {
        boolean isConnect = isInternetConnected(context);
        if (!isConnect) {
        }
        return isConnect;
    }


}
