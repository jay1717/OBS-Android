package com.mywork.jay.obs.BaseClasses.Common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.widget.Toast;

import com.mywork.jay.obs.OBSAplication;

/**
 * Created by jay on 9/1/18.
 */

public class Pref_Common {

    public static Context _context;
    private ProgressDialog progress = null;
    private ProgressDialog mProgress = null;
    private static SharedPreferences pref,pref1;


    @ColorInt
    private static int DEFAULT_TEXT_COLOR = Color.parseColor("#000000");
    @ColorInt
    private static int ERROR_COLOR = Color.parseColor("#F5584D");
    @ColorInt
    private static int INFO_COLOR = Color.parseColor("#B8E1E8");
    @ColorInt
    private static int SUCCESS_COLOR = Color.parseColor("#B8E986");
    @ColorInt
    private static int WARNING_COLOR = Color.parseColor("#FFDB69");
    @ColorInt
    private static int NORMAL_COLOR = Color.parseColor("#353A3E");

    public Pref_Common(Context context) {
        this._context = context;
        pref = OBSAplication.getSharedPreference();
        pref1 = OBSAplication.getSharedPreference1();

    }


    public static String loadPrefString(Context context, String key) {
        // TODO Auto-generated method stub
        String strSaved = pref.getString(key, "");
        return strSaved;
    }

    public static Boolean loadPrefBoolean(Context context, String key) {
        // TODO Auto-generated method stub
        boolean isbool = pref.getBoolean(key, false);
        return isbool;
    }

    public static Boolean loadPrefBooleanExtra(Context context, String key) {
        // TODO Auto-generated method stub
        boolean isbool = pref.getBoolean(key, false);
        return isbool;
    }

    public void savePrefString(Context context, String key, String value) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String savePrefString(String key, String value) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = pref.edit();
        // key encode
        try {
        /*    byte[] data = key.getBytes("UTF-8");
            String data_ley = Base64.encodeToString(data, Base64.DEFAULT);
*/
            // value encode
            value = Base64.encodeToString(value.getBytes(), Base64.DEFAULT);

            editor.putString(key, value);
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }

    public boolean saveArrayData(String arrayName,String[] array) {
        SharedPreferences prefs = _context.getSharedPreferences("preferencename", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(arrayName +"_size", array.length);
        for(int i=0;i<array.length;i++)
            editor.putString(arrayName + "_" + i, array[i]);
        return editor.commit();
    }

    public String[] getArrayData(String arrayName) {
        SharedPreferences prefs = _context.getSharedPreferences("preferencename", 0);
        int size = prefs.getInt(arrayName + "_size", 0);
        String array[] = new String[size];
        for (int i = 0; i < size; i++)
            array[i] = prefs.getString(arrayName + "_" + i, null);
        return array;
    }

    public String savePrefStringExtra(String key, String value) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = pref1.edit();
        // key encode
        try {
            key = Base64.encodeToString(key.getBytes(), Base64.DEFAULT);
            value = Base64.encodeToString(value.getBytes(), Base64.DEFAULT);
            editor.putString(key, value);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return key;
    }

    public void savePrefBoolean(String key, Boolean value) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void savePrefBooleanExtra(String key, Boolean value) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = pref1.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String loadPrefString(String key) {
        // TODO Auto-generated method stub

        String strSaved = pref.getString(key, "");

        try {
            byte[] data = Base64.decode(strSaved, Base64.DEFAULT);
            strSaved = new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strSaved;
    }


    public String loadPrefStringExtra(String key) {
        // TODO Auto-generated method stub

        key = Base64.encodeToString(key.getBytes(), Base64.DEFAULT);

        String strSaved = pref1.getString(key, "");

        try {
            byte[] data = Base64.decode(strSaved, Base64.DEFAULT);
            strSaved = new String(data, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strSaved;
    }

    public Boolean loadPrefBoolean(String key) {
        // TODO Auto-generated method stub
        boolean isbool = pref.getBoolean(key, false);
        return isbool;
    }

    public Boolean loadPrefBooleanExtra(String key) {
        // TODO Auto-generated method stub
        boolean isbool = pref1.getBoolean(key, false);
        return isbool;
    }

    public int loadPrefInt(String key) {
        // TODO Auto-generated method stub
        int iscount = pref.getInt(key, 0);
        return iscount;
    }

    public void savePrefInt(String key, int value) {
        // TODO Auto-generated method stub
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }
  /*  @CheckResult
    public static Toast error(@NonNull CharSequence message, int duration, boolean withIcon) {
        return custom(message, ToastyUtils.getDrawable(_context, R.drawable.cancel_final), ERROR_COLOR, duration, withIcon, true);
    }

    public static Toast success(@NonNull CharSequence message, int duration, boolean withIcon) {
        return custom(message, ToastyUtils.getDrawable(_context, R.drawable.success_final), SUCCESS_COLOR, duration, withIcon, true);
    }

    @CheckResult
    public static Toast warning(@NonNull CharSequence message, int duration, boolean withIcon) {
        return custom(message, ToastyUtils.getDrawable(_context, R.drawable.warning_fianl), WARNING_COLOR, duration, withIcon, true);
    }

    @CheckResult
    public static Toast information(@NonNull CharSequence message, int duration, boolean withIcon) {
        return custom(message, ToastyUtils.getDrawable(_context, R.drawable.information_final), INFO_COLOR, duration, withIcon, true);
    }

    public static Toast custom(@NonNull CharSequence message, Drawable icon, @ColorInt int tintColor, int duration, boolean withIcon, boolean shouldTint) {
        final Toast currentToast = new Toast(_context);
        final View toastLayout = ((LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.toast_layout, null);
        final ImageView toastIcon = (ImageView) toastLayout.findViewById(R.id.toast_icon);
        final TextView toastTextView = (TextView) toastLayout.findViewById(R.id.toast_text);
        Drawable drawableFrame;
        if (shouldTint) drawableFrame = ToastyUtils.tint9PatchDrawableFrame(_context, tintColor);
        else drawableFrame = ToastyUtils.getDrawable(_context, R.drawable.toast_frame);
        ToastyUtils.setBackground(toastLayout, drawableFrame);
        if (withIcon) {
            if (icon == null)
                throw new IllegalArgumentException("Avoid passing 'icon' as null if 'withIcon' is set to true");
            if (tintIcon) icon = ToastyUtils.tintIcon(icon, DEFAULT_TEXT_COLOR);
            ToastyUtils.setBackground(toastIcon, icon);
        } else {
            toastIcon.setVisibility(View.GONE);
        }
        toastTextView.setTextColor(DEFAULT_TEXT_COLOR);
        toastTextView.setText(message);
        toastTextView.setTypeface(currentTypeface);
        toastTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        currentToast.setView(toastLayout);
        currentToast.setDuration(duration); // currentToast.setGravity(Gravity.CENTER, 0, 0); return currentToast;
        return currentToast;
    }*/


    public void showToast(String text) {
        // TODO Auto-generated method stub
     /*   Toast toast = Toast.makeText(_context,text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();*/
        Toast.makeText(_context, text, Toast.LENGTH_LONG).show();
    }

    public void showAlert(String text) {
        // TODO Auto-generated method stub
        try {
            new AlertDialog.Builder(_context).setCancelable(false).setMessage(text).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showAlertWithFinish(String text) {
        // TODO Auto-generated method stub
        new AlertDialog.Builder(_context).setCancelable(false).setMessage(text).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    dialogInterface.dismiss();
                    ((Activity) _context).finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).show();
    }

    public void showAlertCancelable(String text) {
        // TODO Auto-generated method stub
        new AlertDialog.Builder(_context).setCancelable(true).setMessage(text).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

}
