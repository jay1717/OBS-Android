
package com.mywork.jay.obs.Product.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishList {

    @SerializedName("WishList_id")
    @Expose
    private Integer wishListId;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("Products_id")
    @Expose
    private Integer productsId;
    @SerializedName("Products_name")
    @Expose
    private String productsName;
    @SerializedName("brand_name")
    @Expose
    private String brandName;
    @SerializedName("Products_Details")
    @Expose
    private String productsDetails;
    @SerializedName("Products_price")
    @Expose
    private String productsPrice;
    @SerializedName("Products_quanity")
    @Expose
    private String productsQuanity;
    @SerializedName("VendorMaster_id")
    @Expose
    private Integer vendorMasterId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("image_id")
    @Expose
    private Integer imageId;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("is_default")
    @Expose
    private String isDefault;

    public Integer getWishListId() {
        return wishListId;
    }

    public void setWishListId(Integer wishListId) {
        this.wishListId = wishListId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getProductsId() {
        return productsId;
    }

    public void setProductsId(Integer productsId) {
        this.productsId = productsId;
    }

    public String getProductsName() {
        return productsName;
    }

    public void setProductsName(String productsName) {
        this.productsName = productsName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductsDetails() {
        return productsDetails;
    }

    public void setProductsDetails(String productsDetails) {
        this.productsDetails = productsDetails;
    }

    public String getProductsPrice() {
        return productsPrice;
    }

    public void setProductsPrice(String productsPrice) {
        this.productsPrice = productsPrice;
    }

    public String getProductsQuanity() {
        return productsQuanity;
    }

    public void setProductsQuanity(String productsQuanity) {
        this.productsQuanity = productsQuanity;
    }

    public Integer getVendorMasterId() {
        return vendorMasterId;
    }

    public void setVendorMasterId(Integer vendorMasterId) {
        this.vendorMasterId = vendorMasterId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

}
