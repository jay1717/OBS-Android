package com.mywork.jay.obs.Product.Navigation.Activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myrequest.Tostral;
import com.google.gson.Gson;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Common.discretescrollview.DiscreteScrollView;
import com.mywork.jay.obs.BaseClasses.Common.discretescrollview.InfiniteScrollAdapter;
import com.mywork.jay.obs.BaseClasses.Common.discretescrollview.transform.ScaleTransformer;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Adpter.DiscreteScrollViewOptions;
import com.mywork.jay.obs.Product.Adpter.HotDealsDetalAdpter;
import com.mywork.jay.obs.Product.Database.AddtoCart;
import com.mywork.jay.obs.Product.Database.AddtocartDatabase;
import com.mywork.jay.obs.Product.model.HotDealDetailsModel;
import com.mywork.jay.obs.Product.model.ProductImage;
import com.mywork.jay.obs.Product.model.ProductsDetail;
import com.mywork.jay.obs.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HotDealsDetailActivity extends CoreActivity implements DiscreteScrollView.OnItemChangedListener, View.OnClickListener {

    private DiscreteScrollView itemPicker;
    private InfiniteScrollAdapter infiniteAdapter;
    private ImageView imgBack, imgMenu, tv_incr, tv_decr;
    private TextView txt_hotdeal_name,txt_add_cart, tv_update, txtTitle, txt_product_dec, txt_add_wish;
    int minteger = 0, product_id;
    private Pref_Common pref_common;
    String product_name;
    private  boolean setText ;
    private List<ProductImage> productImages = new ArrayList<>();
    private List<ProductsDetail> hotDealDetailsModels = new ArrayList<>();
    AddtocartDatabase addtocartDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_deals_detail);
        pref_common = new Pref_Common(HotDealsDetailActivity.this);
        Bundle bundle = getIntent().getExtras();
        product_id = bundle.getInt("product_id");
        product_name = bundle.getString("product_name");
        Log.e("product_id", "" + product_id);
        setText = pref_common.loadPrefBoolean("setsave");
        addtocartDatabase = new AddtocartDatabase(this);
        setIds();
        setClickEvents();
        setValue();
        GET_PRODUCT_DETAILS();
    }


    private void setIds() {
        try {
            itemPicker = (DiscreteScrollView) findViewById(R.id.item_picker);
            imgBack = (ImageView) findViewById(R.id.img_txt_Back);
            imgMenu = (ImageView) findViewById(R.id.imgMenu);
            tv_incr = (ImageView) findViewById(R.id.activityhot_deals_detail_img_incr);
            tv_decr = (ImageView) findViewById(R.id.activityhot_deals_detail_img_decr);
            txt_add_cart = (TextView) findViewById(R.id.txt_add_cart);
            txt_hotdeal_name = (TextView) findViewById(R.id.txt_hotdeal_name);
            tv_update = (TextView) findViewById(R.id.activityhot_deals_detail_tv_update);
            txtTitle = (TextView) findViewById(R.id.txtTitle);
            txt_product_dec = (TextView) findViewById(R.id.txt_product_dec);
            txt_add_wish = (TextView) findViewById(R.id.txt_add_wish);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickEvents() {
        try {
            imgBack.setOnClickListener(this);
            txt_add_cart.setOnClickListener(this);
            tv_incr.setOnClickListener(this);
            tv_decr.setOnClickListener(this);
            txt_add_wish.setOnClickListener(this);
            txt_hotdeal_name.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setValue() {
        try {
            txtTitle.setText(product_name);
            imgBack.setVisibility(View.VISIBLE);
            imgMenu.setVisibility(View.GONE);
            if (setText == true) {
                txt_add_wish.setText("#UnSave");
            } else if (setText == false) {
                txt_add_wish.setText("#Save");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Display Total Quantity
     **/
    private void display(int number) {
        tv_update.setText("" + number);
    }

    private void setAdpter() {

        try {
            itemPicker.addOnItemChangedListener(this);
            infiniteAdapter = InfiniteScrollAdapter.wrap(new HotDealsDetalAdpter(HotDealsDetailActivity.this, productImages));
            itemPicker.setAdapter(infiniteAdapter);
            itemPicker.setItemTransitionTimeMillis(DiscreteScrollViewOptions.getTransitionTime());
            itemPicker.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_add_cart:
                for (int j = 0; j<hotDealDetailsModels.size();j++){
                    String ProductCate = hotDealDetailsModels.get(j).getBrandName();
                    String ProductId = String.valueOf(hotDealDetailsModels.get(j).getProductId());
                    String ProductName = hotDealDetailsModels.get(j).getProductsName();
                    String cat_id = String.valueOf(hotDealDetailsModels.get(j).getCategoryId());
                    String dec = hotDealDetailsModels.get(j).getProductsDetails();
                    String ProductImage =hotDealDetailsModels.get(j).getProductImage();
                    String ProductPrice =hotDealDetailsModels.get(j).getProductsPrice();
                    String ProductQuantity = String.valueOf(Integer.parseInt(tv_update.getText().toString()));
                    String vendor_id = String.valueOf(hotDealDetailsModels.get(j).getVendorMasterId());

                    AddtoCart addtoCart = new AddtoCart(ProductCate,ProductId,ProductName,cat_id,dec,ProductImage,ProductPrice,ProductQuantity,vendor_id);
                    addtocartDatabase.addProduct(addtoCart);
                    Intent intent = new Intent(HotDealsDetailActivity.this, CartActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.activityhot_deals_detail_img_incr:
                minteger = Integer.parseInt(tv_update.getText().toString());
                minteger = minteger + 1;
                display(minteger);
                break;
            case R.id.activityhot_deals_detail_img_decr:
                minteger = Integer.parseInt(tv_update.getText().toString());
                int tot = minteger - 1;
                if (tot == 0 || tot <= 0) {
                } else {
                    display(tot);
                }
                break;
            case R.id.img_txt_Back:
                finish();
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                break;
            case R.id.txt_add_wish:
                ADD_WISH_LIST_PRODUCT();
                break;
        }
    }

    private void GET_PRODUCT_DETAILS() {
        String getproduct_url = BaseApi.postObsAPI(BaseApi.GET_PRODUCT_DETAILS);
        Map<String, String> param = new HashMap<>();
        param.put("Products_id", String.valueOf(product_id));


        new OBSRequest(HotDealsDetailActivity.this, getproduct_url, OBSRequest.METHOD_POST, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                HotDealDetailsModel hotDealDetailsModel = new Gson().fromJson(response.toString(), HotDealDetailsModel.class);
                productImages = hotDealDetailsModel.getProductImages();

                setAdpter();
                hotDealDetailsModels = hotDealDetailsModel.getProductsDetails();
                for (int k = 0; k < hotDealDetailsModels.size(); k++) {
                    txt_product_dec.setText(hotDealDetailsModels.get(k).getProductsDetails());
                }
            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);
    }

    private void ADD_WISH_LIST_PRODUCT() {
        String add_widh = BaseApi.postObsAPI(BaseApi.ADD_WISH_LIST);

        Map<String, String> param = new HashMap<>();
        param.put("product_id", String.valueOf(product_id));

        new OBSRequest(HotDealsDetailActivity.this, add_widh, OBSRequest.METHOD_PATCH, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getBoolean("status") == true) {
                        txt_add_wish.setText("#UnSave");
                        pref_common.savePrefBoolean("setsave", true);
                        Tostral.success(HotDealsDetailActivity.this, "WishLists created Successfully", Toast.LENGTH_SHORT, true).show();
                    } else if (jsonObject.getBoolean("status") == false) {
                        txt_add_wish.setText("#Save");
                        pref_common.savePrefBoolean("setsave", false);
                        Tostral.success(HotDealsDetailActivity.this, "WishLists deleted Successfully", Toast.LENGTH_SHORT, true).show();
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(addtocartDatabase != null){
            addtocartDatabase.close();
        }
    }

}
