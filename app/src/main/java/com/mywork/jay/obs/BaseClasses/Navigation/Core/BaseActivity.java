package com.mywork.jay.obs.BaseClasses.Navigation.Core;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.transition.ChangeBounds;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.mywork.jay.obs.R;


public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    public static final long MAX_CLICK_INTERVAL = 1000;
    public static String user_id, user_name;
    Slide slideTransition;
    ChangeBounds changeBoundsTransition;
    View header;
    private FragmentManager fragmentManagerBase;
    private ImageView imgMenu_, imgBack;
    private DrawerLayout drawer_;
    private NavigationView navigationView_;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(getLayoutResourceId());

        fragmentManagerBase = getSupportFragmentManager();


        //    setupWindowAnimations();
        hideSoftKeyboard();
    }

    protected void setSearchClick(Class class_name) {
        try {
            startActivity(new Intent(BaseActivity.this, class_name));
            overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
        } catch (Exception e) {
            Log.d("Exception id", "BaseActivity" + e.getMessage());
        }
    }

    public void hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setBaseClick(int menuid) {
        try {
            imgMenu_ = (ImageView) findViewById(R.id.imgMenu);
            drawer_ = (DrawerLayout) findViewById(R.id.drawerLayout);
            navigationView_ = (NavigationView) findViewById(R.id.nav_view);
            setSliderBase(menuid);
            navigationView_.setNavigationItemSelectedListener(this);
            imgMenu_.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MenuClick();
                }
            });
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (fragmentManagerBase.getBackStackEntryCount() == 0) {
                        finish();
                        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                    } else {
                        fragmentManagerBase.popBackStack();

                    }

                }
            });
            try {
                PackageManager manager = getPackageManager();
                PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.d("Exception id", "BaseActivity" + e.getMessage());
        }
    }

    protected void setSliderBase(int menuId) {
        try {
            if (navigationView_ != null) {
                navigationView_.getMenu().clear();
                navigationView_.inflateMenu(menuId);

                navigationView_.removeHeaderView(header);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void MenuClick() {

        if (drawer_.isDrawerOpen(navigationView_)) {
            drawer_.closeDrawer(navigationView_);
        } else {
            drawer_.openDrawer(navigationView_);
        }
    }

    protected void setSliderDashboard(int menuId, String user_name) {
        try {
            if (navigationView_ != null) {
                navigationView_.getMenu().clear();
                navigationView_.inflateMenu(menuId);

                header = LayoutInflater.from(this).inflate(R.layout.user_header, navigationView_, false);
                navigationView_.addHeaderView(header);


            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
          /*  case R.id.nav_logout:
                ALERT_DIALOG_LOGOUT();
                break;*/


        }

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer_.closeDrawer(GravityCompat.START);
        return true;
    }

    private void ALERT_DIALOG_LOGOUT() {
        new AlertDialog.Builder(BaseActivity.this)
                .setTitle("Alert!")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public void push(@NonNull CoreFragment coreFragment, String fragmenttag, Bundle bundleArguments, String prevScreentag, boolean addToStack) {

        setUpAnimation(coreFragment);

        FragmentTransaction ft = getFragmentManagerBase().beginTransaction();

        if (bundleArguments == null)
            bundleArguments = new Bundle();

        coreFragment.setArguments(bundleArguments);

        boolean fragmentPopped = fragmentManagerBase.popBackStackImmediate(fragmenttag, 1);
        //if (!fragmentPopped){
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        ft.add(R.id.fragment, coreFragment, fragmenttag);

        if (addToStack) {
            ft.addToBackStack(fragmenttag);
        } else {
//            ft.addToBackStack(null);
            ft.addToBackStack(fragmenttag);
        }

        ft.commit();
        Log.d("Fragment", "Added");
     /*   }else {
            Log.d("Fragment","Already Added");
        }*/

//        invalidateOptionsMenu();
    }

    public void replace(@NonNull CoreFragment coreFragment, String fragmenttag, Bundle bundleArguments, String prevScreentag, boolean addToStack) {

        setUpAnimation(coreFragment);

        FragmentTransaction ft = getFragmentManagerBase().beginTransaction();

        if (bundleArguments == null)
            bundleArguments = new Bundle();

        coreFragment.setArguments(bundleArguments);

//        ft.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out,R.anim.anim_left_in, R.anim.anim_right_out);

        ft.replace(R.id.fragment, coreFragment, fragmenttag);


        if (addToStack) {
            ft.addToBackStack(prevScreentag);
        } else {
            ft.addToBackStack(null);
        }

        ft.commit();
//        invalidateOptionsMenu();
    }

    private void setUpAnimation(CoreFragment coreFragment) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            slideTransition = new Slide(Gravity.RIGHT);
            slideTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium));
            changeBoundsTransition = new ChangeBounds();
            changeBoundsTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium));

            coreFragment.setEnterTransition(slideTransition);
            coreFragment.setAllowEnterTransitionOverlap(true);
            coreFragment.setAllowReturnTransitionOverlap(true);
            coreFragment.setSharedElementEnterTransition(changeBoundsTransition);
        }
    }


    private void setupWindowAnimations() {
        Fade fade = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            fade = new Fade();
            fade.setDuration(1000);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setEnterTransition(fade);
            }
            Log.d("anim", "start");

            Slide slide = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                slide = new Slide();
            }
            slide.setDuration(1000);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setReturnTransition(slide);
            }
            Log.d("anim", "end");
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public FragmentManager getFragmentManagerBase() {
        return fragmentManagerBase;
    }


    public LinearSnapHelper setFix() {
        LinearSnapHelper snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                View centerView = findSnapView(layoutManager);
                if (centerView == null)
                    return RecyclerView.NO_POSITION;

                int position = layoutManager.getPosition(centerView);
                int targetPosition = -1;
                if (layoutManager.canScrollHorizontally()) {
                    if (velocityX < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                if (layoutManager.canScrollVertically()) {
                    if (velocityY < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }

                final int firstItem = 0;
                final int lastItem = layoutManager.getItemCount() - 1;
                targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                return targetPosition;
            }
        };
//        snapHelper.attachToRecyclerView(recyclerView);
        return snapHelper;
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < MAX_CLICK_INTERVAL) {
            return;
        }
    }
}
