package com.mywork.jay.obs.Product.Database;

/**
 * Created by jay on 21/3/18.
 */

public class AddtoCart {

    String id;
    String productcat;
    String prductname;
    String productid;
    String productimage;
    String productprice;
    String productquantity;
    String productcatId;
    String productcatDec;
    String productvendor_id;


    public AddtoCart(String productCate,String productid, String productName, String cat_id, String dec, String productImage, String productPrice, String productQuantity, String vendor_id) {
        this.productcat = productCate;
        this.prductname = productName;
        this.productid = productid;
        this.productcatId = cat_id;
        this.productcatDec = dec;
        this.productimage = productImage;
        this.productprice = productPrice;
        this.productquantity = productQuantity;
        this.productvendor_id = vendor_id;
    }

    public AddtoCart(String id, String productCate,String productid, String productName, String cat_id, String dec, String productImage, String productPrice, String productQuantity, String vendor_id) {
        this.id = id;
        this.productcat = productCate;
        this.prductname = productName;
        this.productid = productid;
        this.productcatId = cat_id;
        this.productcatDec = dec;
        this.productimage = productImage;
        this.productprice = productPrice;
        this.productquantity = productQuantity;
        this.productvendor_id = vendor_id;
    }

    public String getProductcatId() {
        return productcatId;
    }

    public void setProductcatId(String productcatId) {
        this.productcatId = productcatId;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }
    public String getProductvendor_id() {
        return productvendor_id;
    }

    public void setProductvendor_id(String productvendor_id) {
        this.productvendor_id = productvendor_id;
    }



    public String getProductcatDec() {
        return productcatDec;
    }

    public void setProductcatDec(String productcatDec) {
        this.productcatDec = productcatDec;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductcat() {
        return productcat;
    }

    public void setProductcat(String productcat) {
        this.productcat = productcat;
    }

    public String getPrductname() {
        return prductname;
    }

    public void setPrductname(String prductname) {
        this.prductname = prductname;
    }

    public String getProductimage() {
        return productimage;
    }

    public void setProductimage(String productimage) {
        this.productimage = productimage;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getProductquantity() {
        return productquantity;
    }

    public void setProductquantity(String productquantity) {
        this.productquantity = productquantity;
    }


}
