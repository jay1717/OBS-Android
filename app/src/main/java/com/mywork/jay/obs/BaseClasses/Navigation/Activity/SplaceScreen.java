package com.mywork.jay.obs.BaseClasses.Navigation.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.Product.Navigation.Activity.ProfileActivity;
import com.mywork.jay.obs.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplaceScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 1000;
    private Pref_Common pref_common;
    private ProgressBar prograss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splace_screen);
        pref_common = new Pref_Common(SplaceScreen.this);
        prograss = (ProgressBar)findViewById(R.id.prograss);
        new BackgroundTask().execute();
    }

    private class BackgroundTask extends AsyncTask {
        Intent intent;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pref_common.loadPrefString("islogin").equals("1")){
                intent = new Intent(SplaceScreen.this, DashBoardActivity.class);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                GET_PROFILE_DETAIL();
            }else {
                intent = new Intent(SplaceScreen.this, LoginActivity.class);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
            }
            }

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                Thread.sleep(SPLASH_TIME_OUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
        }
    }

    private void GET_PROFILE_DETAIL() {
        String detail_url = BaseApi.postObsAPI(BaseApi.POST_VENDOR_MASTER);

        Map<String, String> param = new HashMap<>();
        prograss.setVisibility(View.VISIBLE);
        new OBSRequest(SplaceScreen.this, detail_url, OBSRequest.METHOD_GET, false, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                prograss.setVisibility(View.GONE);

                try {
                    JSONObject object = new JSONObject(response.toString());
                    JSONArray jsonArray = object.getJSONArray("VendorMaster");
                    for (int i = 0; i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String business_name = jsonObject.getString("business_name");
                        String business_number = jsonObject.getString("business_number");
                        String business_email = jsonObject.getString("business_email");
                        pref_common.savePrefString("business_name",business_name);
                        pref_common.savePrefString("business_number",business_number);
                        pref_common.savePrefString("business_email",business_email);
                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(String errorData, int statusCode) {
                prograss.setVisibility(View.GONE);
            }
        }).makeRegisterJsonObjectRequest(param);
    }

}
