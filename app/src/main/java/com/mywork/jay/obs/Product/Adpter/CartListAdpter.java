package com.mywork.jay.obs.Product.Adpter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mywork.jay.obs.BaseClasses.util.Log;
import com.mywork.jay.obs.Product.Database.AddtoCart;
import com.mywork.jay.obs.Product.Database.AddtocartDatabase;
import com.mywork.jay.obs.Product.Navigation.Activity.CartActivity;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay on 9/3/18.
 */

public class CartListAdpter extends RecyclerView.Adapter<CartListAdpter.CartListHolder> {
    private Context context;
    private List<AddtoCart> addtoCarts;
    private AddtocartDatabase addtocartDatabase;

    public CartListAdpter(Context context, List<AddtoCart> addtoCarts) {
        this.context = context;
        this.addtoCarts = addtoCarts;
        addtocartDatabase = new AddtocartDatabase(context);
    }

    @Override
    public CartListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_cart_list, parent, false);
        return new CartListAdpter.CartListHolder(view);
    }

    @Override
    public void onBindViewHolder(CartListHolder holder, final int position) {
        Picasso.with(context).load(addtoCarts.get(position).getProductimage()).placeholder(R.drawable.watch).into(holder.cart_image);

        holder.cart_productname.setText(addtoCarts.get(position).getProductcatId());
        holder.cart_brand_name.setText("by "+addtoCarts.get(position).getProductcat());
        holder.cart_quantity.setText(addtoCarts.get(position).getProductquantity());
        holder.txt_cart_price.setText("MYR "+addtoCarts.get(position).getProductprice());
        holder.cart_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addtocartDatabase.deleteProduct(Integer.parseInt(addtoCarts.get(position).getId()));
                ((Activity)context).finish();
                context.startActivity(((Activity) context).getIntent());
            }
        });
    }

    @Override
    public int getItemCount() {
        return addtoCarts.size();
    }

    public class CartListHolder extends RecyclerView.ViewHolder {

        private TextView cart_productname,cart_brand_name,cart_quantity,txt_cart_price;
        private ImageView cart_image,cart_delete;

        public CartListHolder(View itemView) {
            super(itemView);
            cart_productname = (TextView)itemView.findViewById(R.id.cart_productname);
            cart_brand_name = (TextView)itemView.findViewById(R.id.cart_brand_name);
            cart_quantity = (TextView)itemView.findViewById(R.id.cart_quantity);
            txt_cart_price = (TextView)itemView.findViewById(R.id.txt_cart_price);
            cart_image = (ImageView)itemView.findViewById(R.id.cart_image);
            cart_delete = (ImageView)itemView.findViewById(R.id.cart_delete);
        }
    }
}
