package com.mywork.jay.obs.BaseClasses.Navigation.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Fragment.HomeFragment;
import com.mywork.jay.obs.Product.Navigation.Activity.CartActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.ProductMainActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.ProfileActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.Setting;
import com.mywork.jay.obs.R;

public class DashBoardActivity extends CoreActivity implements View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    private ImageView imgMenu,imgcart;
    public NavigationView navigationView;
    public static DashBoardActivity activity;
    private Pref_Common pref_common;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        pref_common = new Pref_Common(DashBoardActivity.this);
        setIds();
        clickEvents();
        initBottomTab();
        setBottomTabSelected(0);
        setSliderDashboard(R.menu.navigation_drawer);
//        setSliderMenu(R.menu.navigation_drawer);
        activity = DashBoardActivity.this;

    }

    /**
     * Bind View
     * */
    public void setIds(){
        try{
            imgMenu = (ImageView)findViewById(R.id.imgMenu);
            imgcart = (ImageView)findViewById(R.id.imgcart);
            drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            setValue();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * set ClickEvents
     * */
    public void clickEvents(){
        try {
            imgMenu.setOnClickListener(this);
            imgcart.setOnClickListener(this);
            imgcart.setVisibility(View.VISIBLE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void setUpHomeFragment() {
//        push(new HomeFragment(), HomeFragment.TAG, null, null, false);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, new HomeFragment(), HomeFragment.class.getSimpleName()).commit();
    }

    /**
     * set Value for the Navigation View
     * */
    public void setValue(){
        try {
            assert navigationView != null;
            navigationView.setNavigationItemSelectedListener(this);
            setNavigationView(navigationView);
            setDrawerLayout(drawer);
            SetImageMenuShow(imgMenu);
            setUpHomeFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgMenu:
                MenuClick();
                break;
            case R.id.imgcart:
                Intent intent = new Intent(DashBoardActivity.this, CartActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.nav_Home:

                break;
            case R.id.hot_deal:
                Intent intent = new Intent(DashBoardActivity.this, HotDealActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_Product:
                Intent product = new Intent(DashBoardActivity.this, ProductMainActivity.class);
                startActivity(product);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_message:
                String business_number = pref_common.loadPrefString("business_number");
                String business_name = pref_common.loadPrefString("business_name");
                String business_email = pref_common.loadPrefString("business_email");
                Log.e("business_number",business_number);


                String smsNumber = "91 " +business_number; // E164 format without '+' sign
                Log.e("smsNumber",smsNumber);

                smsNumber = smsNumber.replace("+", "").replace("", "");
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello " + business_name + "!");
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net"); //phone number without "+" prefix
                sendIntent.setPackage("com.whatsapp");
                if (sendIntent.resolveActivity(DashBoardActivity.this.getPackageManager()) == null) {
                    pref_common.showAlert("WhatsApp not Installed");

                }
                startActivity(sendIntent);
                break;
            case R.id.nav_profile:
                Intent profile = new Intent(DashBoardActivity.this, ProfileActivity.class);
                startActivity(profile);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;
            case R.id.nav_setting:
                Intent setting = new Intent(DashBoardActivity.this, Setting.class);
                startActivity(setting);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                break;

            case R.id.nav_logout:
                ALERT_DIALOG_LOGOUT();
                break;
            case R.id.imgclose:
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void ALERT_DIALOG_LOGOUT() {
        new AlertDialog.Builder(DashBoardActivity.this)
                .setTitle("Alert!")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(DashBoardActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                        pref_common.savePrefString("islogin", "2");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)

                .show();
    }
}
