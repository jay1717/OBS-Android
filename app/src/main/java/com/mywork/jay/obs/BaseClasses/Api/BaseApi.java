package com.mywork.jay.obs.BaseClasses.Api;

/**
 * Created by jay on 19/3/18.
 */

public class BaseApi {

    /**
     * POST API
     * */

    public static final String POST_REGISER = "register";
    public static final String POST_LOGIN = "login";
    public static final String POST_HOT_DEAL= "HotDeals/10";
    public static final String POST_SHOW_CATEGORY= "ShowCategories/10";
    public static final String GET_HOME= "ShowHome/10";
    public static final String GET_PRODUCT_DETAILS= "ProductsDetails/10";
    public static final String ADD_WISH_LIST= "WishList/10";
    public static final String POST_ENQUIRY= "enquiry";
    public static final String POST_VERYFIY= "user/verify";
    public static final String POST_SEARCH= "Search/10";
    public static final String POST_PROFILE_DETAILS= "details";
    public static final String POST_VENDOR_MASTER= "VendorMaster/10";


    public static  String postObsAPI(String endpoint){
        return CommonApi.getObsAPI(endpoint);
    }

}
