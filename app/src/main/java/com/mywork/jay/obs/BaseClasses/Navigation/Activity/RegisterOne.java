package com.mywork.jay.obs.BaseClasses.Navigation.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myrequest.Tostral;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.util.ToastyUtils;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.R;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;


public class RegisterOne extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout ll_sign_up, ll_dealer;
    private EditText edt_username,edt_password,edt_email,edt__con_password,edt_mobile;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private Pref_Common pref_common;
    boolean isDisabled = true;
    private TextView txt_create_veryfy,txt_sign_up, txt_create_acc;
    private String get_one,get_two,get_three,get_four,get_five;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        pref_common = new Pref_Common(RegisterOne.this);
        setIds();
        setClickEvents();
    }

    private void setIds() {
        try {
            edt_mobile = (EditText)findViewById(R.id.edt_mobile_activity_regiseter);
            edt_username = (EditText) findViewById(R.id.edt_username_activity_register);
            edt_email = (EditText)findViewById(R.id.edt_email_activity_regiseter);
            edt_password = (EditText) findViewById(R.id.edt_password_activity_regiser);
            edt__con_password = (EditText) findViewById(R.id.edt__con_password_activity_regiser);
            txt_create_veryfy = (TextView)findViewById(R.id.txt_create_veryfy);
            txt_create_acc = (TextView)findViewById(R.id.txt_create_account);
            txt_sign_up = (TextView)findViewById(R.id.txt_sign_up);
//            ll_sign_up = (LinearLayout)findViewById(R.id.ll_sign_up);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickEvents() {
        try {
            txt_sign_up.setOnClickListener(this);
            txt_create_veryfy.setOnClickListener(this);
            txt_create_acc.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_sign_up:
                getRegiserField();
                break;
            case R.id.txt_create_account:

                Intent intent;
                intent = new Intent(RegisterOne.this, LoginActivity.class);
                startActivity(intent);
                finish();

//                popup();
                break;
        }
    }

    private void getRegiserField(){
        try {
            String name = edt_username.getText().toString();
            String Email = edt_email.getText().toString();
            String mobile = edt_mobile.getText().toString();
            String password = edt_password.getText().toString();
            String con_password = edt__con_password.getText().toString();

            if (name.isEmpty()) {
                pref_common.showAlert("Name cannot be blank!");
            } else if (Email.isEmpty()) {
                pref_common.showAlert("Email cannot be blank!");
            } else if (!Email.matches(emailPattern)) {
                pref_common.showAlert("Email cannot be Valid!");
            } else if (mobile.isEmpty()) {
                pref_common.showAlert("Mobile cannot be blank!");
            } else if (password.isEmpty()) {
                pref_common.showAlert("Password cannot be blank!");
            } else if (con_password.isEmpty()) {
                pref_common.showAlert("Conform password cannot be blank!");
            } else if (!password.equals(con_password)) {
                pref_common.showAlert("Password cannot be match!");
            } else {
                OBS_REGISTER_WEB_CALL(name, Email,mobile,password,con_password);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void OBS_REGISTER_WEB_CALL(String name, String email,String mobile,  String  password, String c_password){

        String register_url = BaseApi.postObsAPI(BaseApi.POST_REGISER);
        Log.e("register_url",register_url);

        Map<String, String> param = new HashMap<>();
        param.put("name", name);
        param.put("email", email);
        param.put("mobile_number", mobile);
        param.put("password", password);
        param.put("c_password", c_password);
        param.put("is_vendor", "0");

        new OBSRequest(RegisterOne.this, register_url, OBSRequest.METHOD_POST, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                txt_create_veryfy.setVisibility(View.GONE);
                Tostral.success(RegisterOne.this,"Register Successfully", Toast.LENGTH_SHORT,true).show();
                startActivity(new Intent(RegisterOne.this, LoginActivity.class));
                finish();
            }

            @Override
            public void onError(String errorData, int statusCode) {
                Log.e("Regiser Error",errorData.toString());

                if (statusCode == 401){
                    Tostral.info(RegisterOne.this,"The email has already been taken", Toast.LENGTH_SHORT,true).show();
                }else if (statusCode == 500){
                    Tostral.error(RegisterOne.this,"Something went wrong. Please try again later.", Toast.LENGTH_SHORT,true).show();
                }
            }
        }).makeRegisterJsonObjectRequest(param);


    }

    public void popup() {

        final Dialog dialog;
        dialog = new Dialog(RegisterOne.this, R.style.PauseDialog);

        /** initialize object for popup **/
        final TextView txt_confrm_otp_;
        final EditText edt_one, edt_two, edt_three, edt_four, edt_five;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        dialog.setContentView(R.layout.veryfied_otp);
        dialog.setCanceledOnTouchOutside(false);
        wmlp.gravity = Gravity.CENTER;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        /** bind view for popup**/
        txt_confrm_otp_ = (TextView)dialog.findViewById(R.id.txt_confrm_otp_);
        edt_one = (EditText) dialog.findViewById(R.id.edt_verification_code_one);
        edt_two = (EditText) dialog.findViewById(R.id.edt_verification_code_two);
        edt_three = (EditText)dialog. findViewById(R.id.edt_verification_code_three);
        edt_four = (EditText) dialog.findViewById(R.id.edt_verification_code_four);
        edt_five = (EditText)dialog. findViewById(R.id.edt_verification_code_five);

        /** set edt auto text*/

        edt_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1){

                    isDisabled = true;
                    txt_confrm_otp_.setBackgroundResource(R.drawable.button_bg_disable);
                    txt_confrm_otp_.setTextColor(ContextCompat.getColor(RegisterOne.this,R.color.colorDisable));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length() == 1) {
                    edt_two.requestFocus();
                }
            }
        });
        edt_two.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1){
                    isDisabled = true;
                    txt_confrm_otp_.setBackgroundResource(R.drawable.button_bg_disable);
                    txt_confrm_otp_.setTextColor(ContextCompat.getColor(RegisterOne.this,R.color.colorDisable));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length() == 1) {
                    edt_three.requestFocus();
                }
            }
        });
        edt_three.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1){
                    isDisabled = true;
                    txt_confrm_otp_.setBackgroundResource(R.drawable.button_bg_disable);
                    txt_confrm_otp_.setTextColor(ContextCompat.getColor(RegisterOne.this,R.color.colorDisable));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 1) {
                    edt_four.requestFocus();
                }
            }
        });
        edt_four.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1){
                    isDisabled = true;
                    txt_confrm_otp_.setBackgroundResource(R.drawable.button_bg_disable);
                    txt_confrm_otp_.setTextColor(ContextCompat.getColor(RegisterOne.this,R.color.colorDisable));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length() == 1) {
                    edt_five.requestFocus();
                }
            }
        });

        edt_five.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 1){
                    isDisabled = false;
                    txt_confrm_otp_.setBackgroundResource(R.drawable.search_bg);
                    txt_confrm_otp_.setTextColor(ContextCompat.getColor(RegisterOne.this,R.color.colorGrey));
                }else {
                    isDisabled = true;
                    txt_confrm_otp_.setBackgroundResource(R.drawable.button_bg_disable);
                    txt_confrm_otp_.setTextColor(ContextCompat.getColor(RegisterOne.this,R.color.colorDisable));
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length() == 1) {
                    edt_five.requestFocus();
                }
            }
        });

        /**
         * get Text
         **/

        txt_confrm_otp_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDisabled){
                    try {
                        get_one = edt_one.getText().toString();
                        get_two = edt_two.getText().toString();
                        get_three = edt_three.getText().toString();
                        get_four = edt_four.getText().toString();
                        get_five = edt_five.getText().toString();
                        String confirmation_code = get_one+get_two+get_three+get_four+get_five;
                        Log.e("confirmation_code",""+confirmation_code);
                        SET_WEB_CALL_CONFORM_OTP(confirmation_code);
                        dialog.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        dialog.show();
    }

    private void SET_WEB_CALL_CONFORM_OTP(String confirmation_code) {

        String sign_url = BaseApi.postObsAPI(BaseApi.POST_VERYFIY);
        Log.e("Register Url", "" + sign_url);

        Map<String, String> param = new HashMap<>();
        param.put("confirmation_code", confirmation_code);


        new OBSRequest(RegisterOne.this, sign_url, OBSRequest.METHOD_POST, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                Log.e("responce",response.toString());
                Toast.makeText(RegisterOne.this,"UserConfirm",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RegisterOne.this, LoginActivity.class));
                finish();
                txt_create_veryfy.setVisibility(View.GONE);
            }

            @Override
            public void onError(String errorData, int statusCode) {
                Log.e("Error",""+errorData);
                if (statusCode == 401){
                    pref_common.showAlert("confirmation code is incorrect ");
                }
            }
        }).makeRegisterJsonObjectRequest(param);
    }



}
