package com.mywork.jay.obs.BaseClasses.Navigation.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myrequest.Tostral;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txt_create_account,txt_login;
    private LinearLayout ll_login;
    private EditText edt_username, edt_password;
    private Pref_Common pref_common;
    private String Token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref_common = new Pref_Common(LoginActivity.this);
        setIds();
        setClickEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        pref_common = new Pref_Common(LoginActivity.this);

    }

    private void setIds() {
        try {
            txt_create_account = (TextView) findViewById(R.id.txt_create_account);
            txt_login = (TextView) findViewById(R.id.txt_login);
            edt_username = (EditText) findViewById(R.id.edt_username_activity_login);
            edt_password = (EditText) findViewById(R.id.edt_password_activity_login);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickEvent() {
        try {
            txt_create_account.setOnClickListener(this);
            txt_login.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getLogInField() {
        try {
            String email = edt_username.getText().toString();
            String pass = edt_password.getText().toString();

            if (email.isEmpty()) {
                pref_common.showAlert("Username cannot be blank!");
            } else if (pass.isEmpty()) {
                pref_common.showAlert("Password cannot be blank!");
            } else {

                LOG_IN_WEB_CALL(email, pass);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_create_account:
                startActivity(new Intent(LoginActivity.this, RegisterOne.class));
                break;
            case R.id.txt_login:
                getLogInField();
                break;
        }
    }

    private void LOG_IN_WEB_CALL(String email, String pass) {
        String login_url = BaseApi.postObsAPI(BaseApi.POST_LOGIN);
        Log.e("login_url", login_url);

        Map<String, String> param = new HashMap<>();
        param.put("email", email);
        param.put("password", pass);

        new OBSRequest(LoginActivity.this, login_url, OBSRequest.METHOD_POST, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject object_user = jsonObject.getJSONObject("success");
                    Token = object_user.getString("token");
                    pref_common.savePrefString("Authorization_Token", Token);
                    pref_common.savePrefString("islogin", "1");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Tostral.success(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT, true).show();
                startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                finish();
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
            }

            @Override
            public void onError(String errorData, int statusCode) {
                Log.e("Login Error", errorData.toString());
                if (statusCode == 401) {
                    Tostral.error(LoginActivity.this, "Incorrect Password and email!", Toast.LENGTH_SHORT, true).show();
                }
            }
        }).makeRegisterJsonObjectRequest(param);
    }
}
