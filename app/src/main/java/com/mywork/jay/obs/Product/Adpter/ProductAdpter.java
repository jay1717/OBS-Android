package com.mywork.jay.obs.Product.Adpter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.mywork.jay.obs.BaseClasses.Adpter.FirstProduct;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Navigation.Activity.CartActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealsDetailActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.ProductMainActivity;
import com.mywork.jay.obs.Product.model.Product;
import com.mywork.jay.obs.Product.model.ProductModel;
import com.mywork.jay.obs.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jay on 5/3/18.
 */

public class ProductAdpter extends RecyclerView.Adapter<ProductAdpter.ProductHolder> {
    private Context context;
    List<ProductModel> getDataAdapter;
    List<Product> productList = new ArrayList<>();
    BillingClient billingClient;
    public ProductAdpter(Context context, ArrayList<ProductModel> productModels, BillingClient billingClient) {
        this.context = context;
        this.getDataAdapter = productModels;
        this.billingClient = billingClient;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_first_product, parent, false);
        return new ProductAdpter.ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int position) {
        productList = getDataAdapter.get(position).getProducts();
        holder.txt_category_name.setText(getDataAdapter.get(position).getCategoryName());

        holder.recyclerViewlayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.rv_product_adpter_first_product.setLayoutManager(holder.recyclerViewlayoutManager);
        holder.rv_product_adpter_first_product.setHasFixedSize(true);
        ProductListAdpter productListAdpter = new ProductListAdpter(context, productList, billingClient);
        holder.rv_product_adpter_first_product.setAdapter(productListAdpter);
//        setAdpter(holder, productList);
    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        private RecyclerView rv_product_adpter_first_product;
        private LinearLayoutManager recyclerViewlayoutManager;
        private TextView txt_category_name;

        public ProductHolder(View itemView) {
            super(itemView);
            rv_product_adpter_first_product = (RecyclerView) itemView.findViewById(R.id.product_adpter_first_product);
            txt_category_name = (TextView) itemView.findViewById(R.id.txt_category_name);
        }
    }

}
