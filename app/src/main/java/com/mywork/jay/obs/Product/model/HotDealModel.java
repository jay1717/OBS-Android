
package com.mywork.jay.obs.Product.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotDealModel {

    @SerializedName("HotDeals")
    @Expose
    private List<HotDeal> hotDeals = null;

    public List<HotDeal> getHotDeals() {
        return hotDeals;
    }

    public void setHotDeals(List<HotDeal> hotDeals) {
        this.hotDeals = hotDeals;
    }

}
