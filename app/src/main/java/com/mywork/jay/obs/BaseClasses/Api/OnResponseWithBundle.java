package com.mywork.jay.obs.BaseClasses.Api;

import android.os.Bundle;

/**
 * Created by rgi-5 on 23/11/16.
 */

public interface OnResponseWithBundle {
    void onSuccess(String response, int statusCode, Bundle bundle);
    void onError(String errorData, int statusCode, Bundle bundle);
}
