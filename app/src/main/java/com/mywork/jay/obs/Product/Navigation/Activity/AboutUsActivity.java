package com.mywork.jay.obs.Product.Navigation.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.R;

public class AboutUsActivity extends CoreActivity implements View.OnClickListener {

    private TextView txtTitle;
    private ImageView img_txt_Back,imgMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        setIds();
        setValue();
        setClickEvents();
    }
    private void setIds(){
        try {
            txtTitle = (TextView)findViewById(R.id.txtTitle);
            imgMenu = (ImageView)findViewById(R.id.imgMenu);
            img_txt_Back = (ImageView)findViewById(R.id.img_txt_Back);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setValue(){
        try {
            txtTitle.setText("About Us");
            img_txt_Back.setVisibility(View.VISIBLE);
            imgMenu.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setClickEvents(){
        try {
            img_txt_Back.setOnClickListener(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_txt_Back:
                finish();
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                break;
        }
    }
}
