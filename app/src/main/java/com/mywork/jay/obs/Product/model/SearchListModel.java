
package com.mywork.jay.obs.Product.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchListModel {

    @SerializedName("ProductsImages")
    @Expose
    private List<ProductsImage> productsImages = null;

    public List<ProductsImage> getProductsImages() {
        return productsImages;
    }

    public void setProductsImages(List<ProductsImage> productsImages) {
        this.productsImages = productsImages;
    }

}
