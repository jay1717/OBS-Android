package com.mywork.jay.obs.BaseClasses.Navigation.Core;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.DashBoardActivity;
import com.mywork.jay.obs.OBSAplication;
import com.mywork.jay.obs.Product.Navigation.Activity.ProductMainActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.ProfileActivity;
import com.mywork.jay.obs.Product.Navigation.Activity.Setting;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;


/**
 * Created by jay on 11/12/17.
 */

public class CoreActivity extends AppCompatActivity {

    public static CoreActivity activity;
    private TabLayout footer_tabLayout;
    public DrawerLayout drawer;
    private ImageView imgMenu;
    public NavigationView navigationView;
    private Pref_Common pref_common;
    private int[] bottomTabIcon = {
            R.drawable.home,
            R.drawable.store,
            R.drawable.chat,
            R.drawable.profile,
            R.drawable.setting,
    };
    private String[] bottompageTitle;
    View header;
    private int currentBottomMenu = -1;
    private FragmentManager fragmentManagerCore;
    private ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        fragmentManagerCore = getSupportFragmentManager();
        activity = CoreActivity.this;
        pref_common = new Pref_Common(CoreActivity.this);

    }

    protected void initBottomTab() {
        footer_tabLayout = (TabLayout) findViewById(R.id.bottom_tab_layout);
        footer_tabLayout.setVisibility(View.VISIBLE);
        footer_tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentFooterTabFragment(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                setCurrentFooterTabFragment(tab.getPosition());

            }
        });

        setBottomTab();

    }

    private void setBottomTab() {
        bottompageTitle = new String[]{getResources().getString(R.string.btab_1), getResources().getString(R.string.btab_2), getResources().getString(R.string.btab_3)
                , getResources().getString(R.string.btab_4),
                getResources().getString(R.string.btab_5)};
        for (int i = 0; i < 5; i++) {
            footer_tabLayout.addTab(footer_tabLayout.newTab().setCustomView(getbTabViewfooter(i)).setTag("FOOTER"));
            footer_tabLayout.setSelectedTabIndicatorColor(getResources().getColor(android.R.color.transparent));
        }
    }

    public View getbTabViewfooter(int position) {
        View v = LayoutInflater.from(this).inflate(R.layout.custom_btab, null);
        TextView tv = (TextView) v.findViewById(R.id.txttitle);
        tv.setText(bottompageTitle[position]);
        img = (ImageView) v.findViewById(R.id.imgicon);
        img.setImageResource(bottomTabIcon[position]);
        return v;
    }

    protected void setBottomTabSelected(int index) {
        currentBottomMenu = index;
        TabLayout.Tab tab = footer_tabLayout.getTabAt(index);
        View view = tab.getCustomView();
        ImageView img = (ImageView) view.findViewById(R.id.imgicon);

        int drawableId = 0;
        switch (index) {
            case 0:
                drawableId = R.drawable.home_active;
                break;

            case 1:
                drawableId = R.drawable.store_active;
                break;

            case 2:
                drawableId = R.drawable.chat_active;
                break;

            case 3:
                drawableId = R.drawable.profile_active;
                break;

            case 4:
                drawableId = R.drawable.setting_active;
                break;

        }

        if (drawableId != 0) {
            img.setImageResource(drawableId);
        }
    }

    public void setCurrentFooterTabFragment(int position) {
        switch (position) {
            case 0:
                Log.e("eE....position...", "  " + position);

                if (currentBottomMenu != 0 && currentBottomMenu != -1) {
                    Intent intent = new Intent(CoreActivity.this, DashBoardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                }
                break;

            case 1:
                if (currentBottomMenu != 1) {
                    Intent intent = new Intent(CoreActivity.this, ProductMainActivity.class);
                    intent.putExtra("isFromBottomMenu", true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                }
                break;
            case 2:
                String business_number = pref_common.loadPrefString("business_number");
                String business_name = pref_common.loadPrefString("business_name");
                String business_email = pref_common.loadPrefString("business_email");
                Log.e("business_number",business_number);


                String smsNumber = "91 " +business_number; // E164 format without '+' sign
                Log.e("smsNumber",smsNumber);

                smsNumber = smsNumber.replace("+", "").replace("", "");
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hello " + business_name + "!");
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net"); //phone number without "+" prefix
                sendIntent.setPackage("com.whatsapp");
                if (sendIntent.resolveActivity(CoreActivity.this.getPackageManager()) == null) {
                    pref_common.showAlert("WhatsApp not Installed");
                    return;
                }
                startActivity(sendIntent);

                break;
            case 3:
                if (currentBottomMenu != 3) {
                    Intent intent = new Intent(CoreActivity.this, ProfileActivity.class);
                    intent.putExtra("isFromBottomMenu", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                }
                break;
            case 4:
                if (currentBottomMenu != 4) {
                    Intent intent = new Intent(CoreActivity.this, Setting.class);
                    intent.putExtra("isFromBottomMenu", true);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                }
                break;
            default:
                break;
        }
    }

    public void setSliderMenu(int menuId) {
        try {
            if (navigationView != null) {
                navigationView.getMenu().clear();
                navigationView.inflateMenu(menuId);

                //   navigationView.removeHeaderView(header);

            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        this.drawer = drawerLayout;
    }

    public void setNavigationView(NavigationView navigationView) {
        this.navigationView = navigationView;
    }

    public void SetImageMenuShow(ImageView imgmenu) {
        this.imgMenu = imgmenu;
    }

    public void setSliderDashboard(int menuId) {
        try {
            if (navigationView != null) {
                navigationView.getMenu().clear();
                navigationView.inflateMenu(menuId);

                header = LayoutInflater.from(this).inflate(R.layout.user_header, navigationView, false);
                navigationView.addHeaderView(header);
                ImageView imgUser = (ImageView) header.findViewById(R.id.img_close_drower);
                imgUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        drawer.closeDrawer(navigationView);
                    }
                });
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void setBaseActivity(DashBoardActivity baseActivity) {
        OBSAplication.getGGApp().setBaseActivity(baseActivity);
    }

    public void MenuClick() {

        if (drawer.isDrawerOpen(navigationView)) {
            drawer.closeDrawer(navigationView);
        } else {
            drawer.openDrawer(navigationView);
        }
    }

    private void openWhatsApp() {
        String smsNumber = "+91 1234567890";
        smsNumber = smsNumber.replace("+", "").replace("", "");

        boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
        if (isWhatsappInstalled) {

            Intent sendIntent = new Intent(Intent.ACTION_SEND);
//            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net");//phone number without "+" prefix

            startActivity(sendIntent);
        } else {
            Uri uri = Uri.parse("market://details?id=com.whatsapp");
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            Toast.makeText(this, "WhatsApp not Installed",
                    Toast.LENGTH_SHORT).show();
            startActivity(goToMarket);
        }
    }

    private boolean whatsappInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

}
