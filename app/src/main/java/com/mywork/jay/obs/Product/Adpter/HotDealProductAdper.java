package com.mywork.jay.obs.Product.Adpter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myrequest.Tostral;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealsDetailActivity;
import com.mywork.jay.obs.Product.model.HotDeal;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jay on 7/3/18.
 */

public class HotDealProductAdper extends RecyclerView.Adapter<HotDealProductAdper.HotDealProductHolder> {
    private Context context;
    private List<HotDeal> hotDeals = new ArrayList<>();

    public HotDealProductAdper(Context context, List<HotDeal> hotDeals) {
        this.context = context;
        this.hotDeals = hotDeals;

    }

    @Override
    public HotDealProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_hot_deal_adper, parent, false);
        return new HotDealProductAdper.HotDealProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final HotDealProductHolder holder, final int position) {

        final HotDeal hotDeal = hotDeals.get(position);
        Picasso.with(context).load(hotDeals.get(position).getHotDealsImage()).placeholder(R.drawable.watch).into(holder.imgDetailShopping);

        if (hotDeal.getProductsName() == null || hotDeals.get(position).getProductsName().equals("null") ||
                hotDeal.getProductsName().equals("") && hotDeal.getBrandName() == null || hotDeals.get(position).getBrandName().equals("null") ||
                hotDeal.getBrandName().equals("")) {
            holder.txtBrand.setText("");
        } else {
            holder.txtBrand.setText(hotDeal.getBrandName() + " " + hotDeal.getProductsName());
        }

        if (hotDeal.getDealPrice() == null || hotDeal.getDealPrice().equals("null") || hotDeal.getDealPrice().equals("")) {
            holder.deal_price_.setText("");
        } else {
            holder.deal_price_.setText("Deal Price:- " + hotDeal.getDealPrice());
        }
        if (hotDeal.getHotDealsEndTime() == null || hotDeal.getHotDealsEndTime().equals("null") || hotDeal.getHotDealsEndTime().equals("")) {
            holder.deal_end_Date.setText("");
        } else {
            holder.deal_end_Date.setText("End Date:- " + hotDeal.getHotDealsEndTime());
        }

        if (hotDeal.getHotDealsDescription() == null || hotDeal.getHotDealsDescription().equals("null") || hotDeal.getHotDealsDescription().equals("")) {
            holder.deal_dec.setText("");
        } else {
            holder.deal_dec.setText(hotDeal.getHotDealsDescription());
        }

        holder.cv_hotdeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HotDealsDetailActivity.class);
                intent.putExtra("product_name", hotDeals.get(position).getProductsName());
                intent.putExtra("product_id", hotDeals.get(position).getProductsId());
                context.startActivity(intent);
            }
        });
        Log.e("product_id", String.valueOf(hotDeals.get(position).getProductsId()));

        holder.img_wish_hart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ADD_WISH_LIST_PRODUCT(hotDeals,String.valueOf(hotDeals.get(position).getProductsId()),holder,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return hotDeals.size();
//        return hotDeals.size();
    }

    public class HotDealProductHolder extends RecyclerView.ViewHolder {
        private TextView txtBrand, deal_price_, deal_end_Date, deal_dec;
        private ImageView imgDetailShopping, img_wish_hart;
        private CardView cv_hotdeals;

        public HotDealProductHolder(View itemView) {
            super(itemView);
            txtBrand = (TextView) itemView.findViewById(R.id.txt_brand_name);
            deal_price_ = (TextView) itemView.findViewById(R.id.strip_hot_deal_adpter_deal_price_);
            deal_end_Date = (TextView) itemView.findViewById(R.id.strip_hot_deal_adpter_end_Date);
            deal_dec = (TextView) itemView.findViewById(R.id.strip_hot_deal_adpte_deal_dec);
            imgDetailShopping = (ImageView) itemView.findViewById(R.id.imgDetailShopping);
            img_wish_hart = (ImageView) itemView.findViewById(R.id.img_wish_hart);
            cv_hotdeals = (CardView) itemView.findViewById(R.id.cv_hotdeals);
        }
    }

    private void ADD_WISH_LIST_PRODUCT(final List<HotDeal> hotDeals, String product_id, final HotDealProductHolder holder, final int position) {
        String add_widh = BaseApi.postObsAPI(BaseApi.ADD_WISH_LIST);

        Map<String, String> param = new HashMap<>();
        param.put("product_id", String.valueOf(product_id));

        new OBSRequest(context, add_widh, OBSRequest.METHOD_PATCH, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {

               /* if (hotDeals.get(position).getIsWished() == true){
                    holder.img_wish_hart.setImageResource(R.drawable.hart_fill);

                }else if (hotDeals.get(position).getIsWished() == true){
                    holder.img_wish_hart.setImageResource(R.drawable.hart_unfill);

                }else {

                }*/

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getBoolean("status") == true) {
                        holder.img_wish_hart.setImageResource(R.drawable.hart_fill);

                    } else if (jsonObject.getBoolean("status") == false) {
                        holder.img_wish_hart.setImageResource(R.drawable.hart_unfill);
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);

    }

}
