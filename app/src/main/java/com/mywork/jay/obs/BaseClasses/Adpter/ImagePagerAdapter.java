package com.mywork.jay.obs.BaseClasses.Adpter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.mywork.jay.obs.BaseClasses.com.antonyt.infiniteviewpager.library.InfinitePagerAdapter;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by indianic on 12/06/17.
 */

public class ImagePagerAdapter extends InfinitePagerAdapter {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private List<String> mList;


    public ImagePagerAdapter(Context context) {
        mContext = context;
//        mInflater = LayoutInflater.from(mContext);
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setDataList(List<String> list) {
        if (list == null || list.size() == 0)
            throw new IllegalArgumentException("list can not be null or has an empty size");
        this.mList = list;
        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View view, ViewGroup container) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = mInflater.inflate(R.layout.home_horizontal_list, container, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        holder.position = position;
        if (!TextUtils.isEmpty(mList.get(position))) {
            Picasso.with(mContext).load(mList.get(position)).placeholder(R.drawable.placeholder_1140x350).error(R.drawable.placeholder_1140x350).into(holder.image);
        } else {
            holder.image.setImageResource(R.drawable.placeholder_1140x350);
        }
        return view;
    }


    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }


    private static class ViewHolder {
        public int position;
        ImageView image;



        public ViewHolder(View view) {
            image = (ImageView) view.findViewById(R.id.imgbusinessitem);
        }
    }
}
