
package com.mywork.jay.obs.Product.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotDealDetailsModel {

    @SerializedName("ProductsDetails")
    @Expose
    private List<ProductsDetail> productsDetails = null;
    @SerializedName("productImages")
    @Expose
    private List<ProductImage> productImages = null;

    public List<ProductsDetail> getProductsDetails() {
        return productsDetails;
    }

    public void setProductsDetails(List<ProductsDetail> productsDetails) {
        this.productsDetails = productsDetails;
    }

    public List<ProductImage> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<ProductImage> productImages) {
        this.productImages = productImages;
    }

}
