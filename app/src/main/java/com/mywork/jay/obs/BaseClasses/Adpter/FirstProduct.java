package com.mywork.jay.obs.BaseClasses.Adpter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Model.HotDealsHome;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Navigation.Activity.HotDealsDetailActivity;
import com.mywork.jay.obs.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bumptech.glide.load.DecodeFormat.PREFER_ARGB_8888;

/**
 * Created by jay on 1/3/18.
 */

public class FirstProduct extends RecyclerView.Adapter<FirstProduct.FirstHomeProductHolder> {
    private Context context;
    List<HotDealsHome> hotDeals;

    public FirstProduct(Context context, List<HotDealsHome> hotDeals) {
        this.context = context;
        this.hotDeals = hotDeals;
    }


    @Override
    public FirstHomeProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strip_home_first, parent, false);
        return new FirstHomeProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final FirstHomeProductHolder holder, final int position) {


        Glide.with(context)
                .load(hotDeals.get(position).getHotDealsImage()).asBitmap().thumbnail(0.1f)
                .placeholder(R.drawable.placeholder_1140x350)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .format(PREFER_ARGB_8888).diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.strip_home_first);

        holder._product_name.setText(hotDeals.get(position).getProductsName());
        holder.ll_strip_home_first_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HotDealsDetailActivity.class);
                intent.putExtra("product_name", hotDeals.get(position).getProductsName());
                intent.putExtra("product_id", hotDeals.get(position).getProductsId());
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });
        holder.img_wish_hart.setVisibility(View.INVISIBLE);
        holder.img_wish_hart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ADD_WISH_LIST_PRODUCT(String.valueOf(hotDeals.get(position).getProductsId()), holder, position);
            }
        });
        if (hotDeals.get(position).getIsWished() == true) {
            holder.img_wish_hart.setImageResource(R.drawable.delete_final_bill);
        } else if (hotDeals.get(position).getIsWished() == false) {
            holder.img_wish_hart.setImageResource(R.drawable.delete_final_bill);
        }
        if ( hotDeals.get(position).getDealPrice() == null || hotDeals.get(position).getDealPrice().equals("null")) {
            holder.product_deal_price.setText("MYR :- 00");
        } else {
            holder.product_deal_price.setText("MYR :- " + hotDeals.get(position).getDealPrice());
        }
        if ( hotDeals.get(position).getDealDiscount() == null || hotDeals.get(position).getDealDiscount().equals("null")) {
            holder.product_price.setText("MYR :- 00");
        } else {
            holder.product_price.setText("MYR :- " + hotDeals.get(position).getDealDiscount());
        }

    }

    @Override
    public int getItemCount() {
        return hotDeals.size();
    }

    public void notifyData(List<HotDealsHome> myList) {
        Log.d("notifyData ", myList.size() + "");
        this.hotDeals = myList;
        notifyDataSetChanged();
    }

    public class FirstHomeProductHolder extends RecyclerView.ViewHolder {
        private TextView _product_name, product_deal_price, product_price;
        private ImageView strip_home_first, img_wish_hart;
        private RelativeLayout ll_strip_home_first_;
        private ProgressBar circular_progress;

        public FirstHomeProductHolder(View itemView) {
            super(itemView);
            _product_name = (TextView) itemView.findViewById(R.id.txt_strip_home_first_product_name);
            product_deal_price = (TextView) itemView.findViewById(R.id.txt_strip_home_first_product_deal_price);
            product_price = (TextView) itemView.findViewById(R.id.txt_strip_home_first_product_price);
            strip_home_first = (ImageView) itemView.findViewById(R.id.strip_home_first);
            img_wish_hart = (ImageView) itemView.findViewById(R.id.img_wish_hart);
            ll_strip_home_first_ = (RelativeLayout) itemView.findViewById(R.id.ll_strip_home_first_);
            circular_progress = (ProgressBar)itemView.findViewById(R.id.circular_progress);
        }
    }

    private void ADD_WISH_LIST_PRODUCT(String product_id, final FirstProduct.FirstHomeProductHolder holder, final int position) {
        String add_widh = BaseApi.postObsAPI(BaseApi.ADD_WISH_LIST);

        Map<String, String> param = new HashMap<>();
        param.put("product_id", String.valueOf(product_id));
        holder.circular_progress.setVisibility(View.VISIBLE);
        new OBSRequest(context, add_widh, OBSRequest.METHOD_PATCH, false, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                holder.circular_progress.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getBoolean("status") == true) {
                        Toast.makeText(context, "Add to wishlist", Toast.LENGTH_SHORT).show();
                        holder.img_wish_hart.setImageResource(R.drawable.hart_fill);

                    } else if (jsonObject.getBoolean("status") == false) {
                        holder.img_wish_hart.setImageResource(R.drawable.hart_unfill);
                        Toast.makeText(context, "Remove from wishlist", Toast.LENGTH_SHORT).show();
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String errorData, int statusCode) {
                holder.circular_progress.setVisibility(View.GONE);
            }
        }).makeRegisterJsonObjectRequest(param);

    }

}
