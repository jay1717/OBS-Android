package com.mywork.jay.obs.BaseClasses.Api;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Common.Utility.NetworkUtils;
import com.mywork.jay.obs.BaseClasses.Common.Utility.ProgressDialogUtils;
import com.mywork.jay.obs.OBSAplication;
import com.mywork.jay.obs.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jay on 21/11/17.
 */

public class OBSRequest {

    public static final int METHOD_GET = Request.Method.GET;
    public static final int METHOD_PUT = Request.Method.PUT;
    public static final int METHOD_POST = Request.Method.POST;
    public static final int METHOD_DELETE = Request.Method.DELETE;
    public static final int METHOD_PATCH = Request.Method.PATCH;

    Pref_Common prf;
    StringRequest stringRequest;
    JsonArrayRequest jsonArrayRequest;
    JsonObjectRequest jsonObjectRequest;
    CustomRequest customRequest;
    StringRequest postRequest;

    private String URL;
    private int METHOD;
    private JSONObject jsonObjParam = null;
    private JSONArray jsonArrayParam = null;
    private boolean showProgress;
    private OnResponse onResponse;
    private OnResponseWithBundle onResponseWithBundle;
    private Context context;
    private ProgressDialogUtils progressDialogUtils;
    private int STATUS_CODE;
    private Bundle bundle;
    private Dialog dialogs = null;

    public OBSRequest(Context context, String url, int method, boolean showProgress, JsonObject jsonObject, Bundle bundle, OnResponseWithBundle onResponseWithBundle){
        prf = new Pref_Common(context);
        URL = url;
        METHOD = method;
        this.showProgress = showProgress;
        this.jsonObjParam = jsonObjParam;
        this.onResponseWithBundle = onResponseWithBundle;
        this.onResponse = null;
        this.context = context;
        this.bundle = bundle;
        dialogs = new Dialog(context);
        progressDialogUtils = new ProgressDialogUtils(context);
    }

    public OBSRequest(Context context, String url, int method, boolean showProgress, JSONObject jsonObjParam, OnResponse onResponse) {
        prf = new Pref_Common(context);
        URL = url;
        METHOD = method;
        this.showProgress = showProgress;
        this.jsonObjParam = jsonObjParam;
        this.onResponse = onResponse;
        this.context = context;
        this.bundle = null;
        dialogs = new Dialog(context);
        progressDialogUtils = new ProgressDialogUtils(context);
    }

    public OBSRequest(Context context, String url, int method, boolean showProgress, JSONArray jsonArrayParam, Bundle bundle, OnResponseWithBundle onResponseWithBundle) {
        prf = new Pref_Common(context);
        URL = url;
        METHOD = method;
        this.showProgress = showProgress;
        this.jsonArrayParam = jsonArrayParam;
        this.onResponseWithBundle = onResponseWithBundle;
        this.onResponse = null;
        this.context = context;
        this.bundle = bundle;
        dialogs = new Dialog(context);
        progressDialogUtils = new ProgressDialogUtils(context);

    }

    public OBSRequest(Context context, String url, int method, boolean showProgress, JSONArray jsonArrayParam, OnResponse onResponse) {
        prf = new Pref_Common(context);
        URL = url;
        METHOD = method;
        this.showProgress = showProgress;
        this.jsonArrayParam = jsonArrayParam;
        this.onResponse = onResponse;
        this.context = context;
        this.bundle = null;
        dialogs = new Dialog(context);
        progressDialogUtils = new ProgressDialogUtils(context);
    }

    public OBSRequest(Context context, String url, int method, boolean showProgress, JSONObject jsonRequest, OnResponse onResponse, boolean isArray) {
        prf = new Pref_Common(context);
        URL = url;
        METHOD = method;
        this.showProgress = showProgress;
        this.jsonObjParam = jsonRequest;
        this.onResponse = onResponse;
        this.context = context;
        this.bundle = null;
        dialogs = new Dialog(context);
        progressDialogUtils = new ProgressDialogUtils(context);
    }

    public void makeJsonCustomObjectToArray(){
        try {

            if (!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();
            customRequest = new CustomRequest(METHOD, URL, jsonObjParam, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.e("VOLLY_RESPONSE", "response : " + response);
                    dismisProgress();
                    if(onResponse != null){
                        onResponse.onSuccess(response.toString(), STATUS_CODE);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    dismisProgress();
                    Log.d("Response error",error.getMessage());
                    try {
                        Log.e("VOLLY_RESPONSE", "error message : " + error.getMessage());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            addRequest(customRequest);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void makeJsonObjectRequestWithArrayReponse() {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();

            jsonObjectRequest = new JsonObjectRequest(METHOD, URL, jsonObjParam,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismisProgress();
                            Log.e("VOLLY_RESPONSE", "response : " + response);


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dismisProgress();

                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    //  String credentials = "username:password";
                    //   String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    //   headers.put("Content-Type", "application/json");

                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                    dismisProgress();

                    STATUS_CODE = response.statusCode;
                    Log.e("VOLLY_RESPONSE", "statusCode : " + STATUS_CODE);

                    try {
                        if (STATUS_CODE == HTTPStatusCodes.CODE_200 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_201 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_304) {

                            String json = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));

                            if(!json.contains("{") || !json.contains("}")){
                                return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                            }
                            else{
                                return Response.success(new JSONObject(json), HttpHeaderParser.parseCacheHeaders(response));
                            }
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_204) {
                            return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_422) {
                            return Response.error(new ParseError(response));
                        }
                        else {
                            showErrorMessage();
                            return null;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return Response.error(new ParseError(response));
                    }
                }
                private void showErrorMessage() {
                    try {
                        String message = context.getResources().getString(R.string.error_message_unexpected);

                        switch (STATUS_CODE) {
                            case HTTPStatusCodes.CODE_400:
                                message = context.getResources().getString(R.string.error_message_for_400);
                                break;

                            case HTTPStatusCodes.CODE_401:
                                message = context.getResources().getString(R.string.error_message_for_401);
                                break;

                            case HTTPStatusCodes.CODE_403:
                                message = context.getResources().getString(R.string.error_message_for_403);
                                break;

                            case HTTPStatusCodes.CODE_404:
                                message = context.getResources().getString(R.string.error_message_for_404);
                                break;

                            case HTTPStatusCodes.CODE_405:
                                message = context.getResources().getString(R.string.error_message_for_405);
                                break;

                            case HTTPStatusCodes.CODE_410:
                                message = context.getResources().getString(R.string.error_message_for_410);
                                break;

                            case HTTPStatusCodes.CODE_415:
                                message = context.getResources().getString(R.string.error_message_for_415);
                                break;

                            case HTTPStatusCodes.CODE_429:
                                message = context.getResources().getString(R.string.error_message_for_429);
                                break;

                            default:
                                message = context.getResources().getString(R.string.error_message_unexpected);
                                break;

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            addRequest(jsonObjectRequest);

        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeStringRequest() {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();

            stringRequest = new StringRequest(METHOD, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dismisProgress();
                    Log.e("VOLLY_RESPONSE", "response : " + response);
                    if(onResponse != null){
                        onResponse.onSuccess(response.toString(), STATUS_CODE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dismisProgress();
                    try {
                        Log.e("VOLLY_RESPONSE", "error message : " + error.getMessage());
                        String jsonErrorData = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));

                        Log.e("VOLLY_RESPONSE", "error data : " + jsonErrorData);

                        STATUS_CODE=error.networkResponse.statusCode;

                            /*    if (STATUS_CODE== HTTPStatusCodes.CODE_401) {
                                    Toast.makeText(context,"Your username/password was incorrect, please try again!",Toast.LENGTH_LONG).show();
                                }*/

                        if (STATUS_CODE== HTTPStatusCodes.CODE_401) {




                        }

                        if(onResponse != null){
                            onResponse.onError(jsonErrorData, STATUS_CODE);
                        }



                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    //  String credentials = "username:password";
                    //   String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    //   headers.put("Content-Type", "application/json");


                    return headers;
                }
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {

                    dismisProgress();

                    STATUS_CODE = response.statusCode;
                    Log.e("VOLLY_RESPONSE", "statusCode : " + STATUS_CODE);

                    try {
                        if (STATUS_CODE == HTTPStatusCodes.CODE_200 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_201 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_304) {

                            String json = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));
                            return null;
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_204) {
                            return null;
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_422) {
                            return Response.error(new ParseError(response));
                        } else {
                            showErrorMessage();
                            return null;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return Response.error(new ParseError(response));
                    }
                }
            };

                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }*/

                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params =  super.getHeaders();
                    if(params==null)params = new HashMap<>();
                    params.put("Authorization","Your authorization");
                    return params;
                }*/

            addRequest(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeformdataRequest(){
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void makeJsonObjectRequest() {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();
            Log.d("request",jsonObjParam.toString());
            jsonObjectRequest = new JsonObjectRequest(METHOD, URL, jsonObjParam,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismisProgress();
                                Log.e("VOLLY_RESPONSE", "response : " + response);
                            if(onResponse != null){
                                onResponse.onSuccess(response.toString(), STATUS_CODE);
                            }
                            if(onResponseWithBundle != null){
                                onResponseWithBundle.onSuccess(response.toString(), STATUS_CODE, bundle);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dismisProgress();

                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Accept", "application/json");
                    headers.put("Authorization","Bearer "+ prf.loadPrefString("Authorization_Token"));
                    headers.put("Content-Type","application/x-www-form-urlencoded");
                    //  String credentials = "username:password";
                    //   String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);


                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                    dismisProgress();

                    STATUS_CODE = response.statusCode;
                    //     Log.e("VOLLY_RESPONSE", "statusCode : " + STATUS_CODE);

                    try {
                        if (STATUS_CODE == HTTPStatusCodes.CODE_200 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_201 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_304) {

                            String json = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));

                            if(!json.contains("{") || !json.contains("}")){
                                return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                            }
                            else{
                                return Response.success(new JSONObject(json), HttpHeaderParser.parseCacheHeaders(response));
                            }
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_204) {
                            return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_422) {
                            return Response.error(new ParseError(response));
                        }
                        else {
                            showErrorMessage();
                            return null;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return Response.error(new ParseError(response));
                    }
                }
            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            addRequest(jsonObjectRequest);


        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeRegisterJsonObjectRequest(final Map<String,String> paramss) {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();
            Log.d("request hash",paramss.toString());
            stringRequest = new StringRequest(METHOD, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("response",response);
                    dismisProgress();
                    if(onResponse != null){
                        onResponse.onSuccess(response, STATUS_CODE);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dismisProgress();
                    Log.d("Responce error",""+error);
                    try {
                            String jsonErrorData = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));
                            STATUS_CODE = error.networkResponse.statusCode;
                            if (onResponse != null) {
                                onResponse.onError(jsonErrorData, STATUS_CODE);
                            }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }){
                @Override
                public String getBodyContentType()
                {
                    return "application/x-www-form-urlencoded";
                }
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<>();
//                    paramss.put("token", "cbsdbcsdbcsdcbsbc");
//                    params.put("email","jaimin.developer@gmail.com");
                    return paramss;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Accept", "application/json");
                    params.put("Authorization","Bearer "+ prf.loadPrefString("Authorization_Token"));
                    params.put("Content-Type","application/x-www-form-urlencoded");
//                    params.put("Content-Type","application/x-www-form-urlencoded");
//                    params.put("X-Requested-With","XMLHttpRequest");
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            addRequest(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeloginRegisterJsonObjectRequest(final Map<String,String> paramss) {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();
            Log.d("request hash",paramss.toString());
            stringRequest = new StringRequest(METHOD, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("response",response);
                    dismisProgress();
                    if(onResponse != null){
                        onResponse.onSuccess(response, STATUS_CODE);
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dismisProgress();
                    Log.d("Responce error",""+error);
                    try {
                        Log.e("VOLLY_RESPONSE", "error message : " + error.networkResponse.statusCode);
                        Log.e("VOLLY_RESPONSE", "error message : " + error.getMessage());
                        String jsonErrorData = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));
                        STATUS_CODE = error.networkResponse.statusCode;
                        if (onResponse != null) {
                            onResponse.onError(jsonErrorData, STATUS_CODE);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }){
                @Override
                public String getBodyContentType()
                {
                    return "application/x-www-form-urlencoded";
                }
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String>  params = new HashMap<>();
//                    paramss.put("token", "cbsdbcsdbcsdcbsbc");
//                    params.put("email","jaimin.developer@gmail.com");
                    return paramss;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                   /* params.put("Accept", "application/json");
                    params.put("Authorization", prf.loadPrefString("Authorization_Token"));*/
//                    params.put("Content-Type","application/x-www-form-urlencoded");
//                    params.put("X-Requested-With","XMLHttpRequest");
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            addRequest(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeJsonObjectRequestGGStat() {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();
            jsonObjectRequest = new JsonObjectRequest(METHOD, URL, jsonObjParam,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismisProgress();
                            //    Log.e("VOLLY_RESPONSE", "response : " + response);
                            if(onResponse != null){
                                onResponse.onSuccess(response.toString(), STATUS_CODE);
                            }
                            if(onResponseWithBundle != null){
                                onResponseWithBundle.onSuccess(response.toString(), STATUS_CODE, bundle);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dismisProgress();

                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    //  String credentials = "username:password";
                    //   String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    //   headers.put("Content-Type", "application/json");

                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                    dismisProgress();

                    STATUS_CODE = response.statusCode;
                    //     Log.e("VOLLY_RESPONSE", "statusCode : " + STATUS_CODE);

                    try {
                        if (STATUS_CODE == HTTPStatusCodes.CODE_200 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_201 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_304) {

                            String json = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));

                            if(!json.contains("{") || !json.contains("}")){
                                return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                            }
                            else{
                                return Response.success(new JSONObject(json), HttpHeaderParser.parseCacheHeaders(response));
                            }
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_204) {
                            return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_422) {
                            return Response.error(new ParseError(response));
                        }
                        else {
                            showErrorMessage();
                            return null;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return Response.error(new ParseError(response));
                    }
                }
            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            addRequest(jsonObjectRequest);


        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeJsonObjectRequest(String dialog_message) {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            Log.d("jsonObj",jsonObjParam.toString());
            showProgress(dialog_message);
            jsonObjectRequest = new JsonObjectRequest(METHOD, URL, jsonObjParam,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismisProgress();
                            Log.e("VOLLY_RESPONSE", "response : " + response);
                            if(onResponse != null){
                                onResponse.onSuccess(response.toString(), STATUS_CODE);
                            }
                            if(onResponseWithBundle != null){
                                onResponseWithBundle.onSuccess(response.toString(), STATUS_CODE, bundle);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dismisProgress();

                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    //  String credentials = "username:password";
                    //   String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    //   headers.put("Content-Type", "application/json");


                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                    dismisProgress();

                    STATUS_CODE = response.statusCode;
                    Log.e("VOLLY_RESPONSE", "statusCode : " + STATUS_CODE);

                    try {
                        if (STATUS_CODE == HTTPStatusCodes.CODE_200 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_201 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_304) {

                            String json = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));

                            if(!json.contains("{") || !json.contains("}")){
                                return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                            }
                            else{
                                return Response.success(new JSONObject(json), HttpHeaderParser.parseCacheHeaders(response));
                            }
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_204) {
                            return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_422) {
                            return Response.error(new ParseError(response));
                        } else {
                            showErrorMessage();
                            return null;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return Response.error(new ParseError(response));
                    }
                }
            };
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            addRequest(jsonObjectRequest);

        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeJsonArrayRequest() {
        try {
            if(!NetworkUtils.isNetworkAvailable(context)){
                return;
            }
            showProgress();
            jsonArrayRequest = new JsonArrayRequest(METHOD, URL, jsonArrayParam,

                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            dismisProgress();

                            if(onResponse != null){
                                onResponse.onSuccess(response.toString(), STATUS_CODE);
                            }
                            if(onResponseWithBundle != null){
                                onResponseWithBundle.onSuccess(response.toString(), STATUS_CODE, bundle);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            dismisProgress();


                        }
                    }) {
                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    dismisProgress();
                    STATUS_CODE = response.statusCode;
                    Log.e("VOLLY_RESPONSE", "statusCode : " + STATUS_CODE);
                    try {
                        if (STATUS_CODE == HTTPStatusCodes.CODE_200 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_201 ||
                                STATUS_CODE == HTTPStatusCodes.CODE_304) {
                            String json = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers));
                            return Response.success(new JSONArray(json), HttpHeaderParser.parseCacheHeaders(response));
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_204) {
                            return Response.success(new JSONArray(), HttpHeaderParser.parseCacheHeaders(response));
                        } else if (STATUS_CODE == HTTPStatusCodes.CODE_422) {
                            return Response.error(new ParseError(response));
                        }
                        else {
                            showErrorMessage();
                            return null;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return Response.error(new ParseError(response));
                    }
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();


                    return headers;
                }
            };
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            addRequest(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
            dismisProgress();
        }
    }

    public void makeRequestPaymentJSON(final String username,final String domain,final String amount,final String order) {
        try {
            if (!NetworkUtils.isNetworkAvailable(context)) {
                return;
            }
            showProgress();

            postRequest = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            dismisProgress();
                            if(onResponse != null){
                                onResponse.onSuccess(response.toString(), STATUS_CODE);
                            }

                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            dismisProgress();

                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();


                    return headers;
                }
            };
            addRequest(postRequest);
        }catch (Exception e){
            e.printStackTrace();
            dismisProgress();
        }
    }

    private void showErrorMessage() {
        try {
            String message = context.getResources().getString(R.string.error_message_unexpected);

            switch (STATUS_CODE) {
                case HTTPStatusCodes.CODE_400:
                    message = context.getResources().getString(R.string.error_message_for_400);
                    break;

                case HTTPStatusCodes.CODE_401:
                    message = context.getResources().getString(R.string.error_message_for_401);
                    break;

                case HTTPStatusCodes.CODE_403:
                    message = context.getResources().getString(R.string.error_message_for_403);
                    break;

                case HTTPStatusCodes.CODE_404:
                    message = context.getResources().getString(R.string.error_message_for_404);
                    break;

                case HTTPStatusCodes.CODE_405:
                    message = context.getResources().getString(R.string.error_message_for_405);
                    break;

                case HTTPStatusCodes.CODE_410:
                    message = context.getResources().getString(R.string.error_message_for_410);
                    break;

                case HTTPStatusCodes.CODE_415:
                    message = context.getResources().getString(R.string.error_message_for_415);
                    break;

                case HTTPStatusCodes.CODE_429:
                    message = context.getResources().getString(R.string.error_message_for_429);
                    break;

                default:
                    message = context.getResources().getString(R.string.error_message_unexpected);
                    break;

            }

//            GAlert.showToastMessage(context, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* private void buildDialog(int dialogTheme, String s) {
        if (dialogs != null && dialogs.isShowing()){

        }else {

            dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogs.setContentView(R.layout.service_unavailable_dialouge);
            dialogs.setCanceledOnTouchOutside(true);
            dialogs.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialogs.getWindow().setGravity(Gravity.CENTER);
            TextView tv_close = (TextView) dialogs.findViewById(R.id.tv_close);
            TextView txt_coming = (TextView) dialogs.findViewById(R.id.txt_coming);
            //String st= Html.fromHtml("<sup><small>st</small></sup>");
            //txt_coming.setText("COMING 1st MAY, 2017");
            txt_coming.setVisibility(View.GONE);
            tv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogs.dismiss();
                }
            });
            dialogs.getWindow().getAttributes().windowAnimations = dialogTheme;
            if (!dialogs.isShowing()){
                dialogs.show();

            }

        }
    }*/

    private void showProgress() {
        try {
            if (showProgress) {
                progressDialogUtils.showLoadingProgress();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgress(String message) {
        try {
            if (showProgress) {
                progressDialogUtils.showLoadingProgress(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismisProgress() {
        try {
            if (showProgress && progressDialogUtils != null) {
                progressDialogUtils.dismissProgress();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private <T> void addRequest(Request<T> req) {
        OBSAplication.getGGApp().addToRequestQueue(req);
    }
}
