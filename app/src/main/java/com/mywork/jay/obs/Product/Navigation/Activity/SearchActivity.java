package com.mywork.jay.obs.Product.Navigation.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mywork.jay.obs.BaseClasses.Api.BaseApi;
import com.mywork.jay.obs.BaseClasses.Api.OBSRequest;
import com.mywork.jay.obs.BaseClasses.Api.OnResponse;
import com.mywork.jay.obs.BaseClasses.Common.Pref_Common;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.LoginActivity;
import com.mywork.jay.obs.BaseClasses.Navigation.Activity.RegisterOne;
import com.mywork.jay.obs.BaseClasses.Navigation.Core.CoreActivity;
import com.mywork.jay.obs.BaseClasses.util.permission.Log;
import com.mywork.jay.obs.Product.Adpter.SearchAdpter;
import com.mywork.jay.obs.Product.model.ProductsImage;
import com.mywork.jay.obs.Product.model.SearchListModel;
import com.mywork.jay.obs.R;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends CoreActivity implements View.OnClickListener {

    private RecyclerView rv_activity_search_;
    private EditText edt_search;
    private ImageView img_search,imgMenu,imgBack;
    private TextView txt_search_no_data;
    private LinearLayout ll_search;
    private SearchAdpter searchAdpter;
    private GridLayoutManager recyclerViewlayoutManager;
    private List<ProductsImage> productsImages = new ArrayList<>();
    private Pref_Common pref_common;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        pref_common = new Pref_Common(SearchActivity.this);
        setIds();
        setclickEvents();
        setLayoutmanager();
    }
    private void setIds(){
        try {
            rv_activity_search_ = (RecyclerView)findViewById(R.id.rv_activity_search_);
            edt_search = (EditText) findViewById(R.id.edt_search);
            img_search = (ImageView) findViewById(R.id.img_search);
            imgBack = (ImageView) findViewById(R.id.imgBack);
            imgMenu = (ImageView) findViewById(R.id.imgMenu);
            ll_search = (LinearLayout) findViewById(R.id.ll_search);
            txt_search_no_data = (TextView)findViewById(R.id.txt_search_no_data);
            imgBack.setVisibility(View.VISIBLE);
            imgMenu.setVisibility(View.GONE);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setLayoutmanager(){
        try {

            recyclerViewlayoutManager = new GridLayoutManager(SearchActivity.this, LinearLayoutManager.VERTICAL);
            recyclerViewlayoutManager.setSpanCount(2);
            rv_activity_search_.setLayoutManager(recyclerViewlayoutManager);
            rv_activity_search_.setHasFixedSize(true);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setclickEvents(){
        try {
            ll_search.setOnClickListener(this);
            edt_search.setOnClickListener(this);
            img_search.setOnClickListener(this);
            imgBack.setOnClickListener(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
           case  R.id.img_search:
               String name = edt_search.getText().toString();
               if (name.isEmpty()){
                   pref_common.showAlert("Search can not be blank ");
               }else {
                   SET_SEARCH_CALL(name);
               }
            break;
            case R.id.imgBack :
                finish();
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_right_out);
                break;
        }
    }
    private void SET_SEARCH_CALL(String confirmation_code){
        String search_url = BaseApi.postObsAPI(BaseApi.POST_SEARCH);

        Map<String, String> param = new HashMap<>();
        param.put("name", confirmation_code);

        new OBSRequest(SearchActivity.this, search_url, OBSRequest.METHOD_POST, true, new JSONObject(), new OnResponse() {
            @Override
            public void onSuccess(String response, int statusCode) {
                SearchListModel searchListModel = new Gson().fromJson(response.toString(),SearchListModel.class);
                productsImages = searchListModel.getProductsImages();

                if (productsImages.isEmpty()){
                    txt_search_no_data.setVisibility(View.VISIBLE);
                    rv_activity_search_.setVisibility(View.GONE);
                }else {
                    txt_search_no_data.setVisibility(View.GONE);
                    rv_activity_search_.setVisibility(View.VISIBLE);
                    setsearchadptr();
                }
            }

            @Override
            public void onError(String errorData, int statusCode) {

            }
        }).makeRegisterJsonObjectRequest(param);
    }

    private void setsearchadptr(){
        try {
            searchAdpter = new SearchAdpter(SearchActivity.this, productsImages);
            rv_activity_search_.setAdapter(searchAdpter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}

